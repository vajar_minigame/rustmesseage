-- Your SQL goes here
CREATE TABLE sub_user_room
(
    room_id INTEGER NOT NULL REFERENCES room(id),
    user_id INTEGER NOT NULL ,
    entrance_time TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(room_id, user_id)

);