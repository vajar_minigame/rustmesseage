-- Your SQL goes here
CREATE TABLE room
(
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    description TEXT NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT NOW()
)