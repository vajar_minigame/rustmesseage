-- Your SQL goes here
CREATE TABLE messages
(
    id SERIAL PRIMARY KEY,
    body Text NOT NULL,
    sender integer NOT NULL,
    sent_time TIMESTAMP NOT NULL,
    room_id INTEGER REFERENCES room(id) NOT NULL
)