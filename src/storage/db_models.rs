#[derive(Queryable)]
pub struct Room {
    pub id: i32,
    pub name: String,
    pub description: String,
    pub created: std::time::SystemTime,
}

#[derive(Queryable, Debug)]
pub struct Message {
    pub id: i32,
    pub body: String,
    pub sender: i32,
    pub sent_time: std::time::SystemTime,
    pub room_id: i32,
}

use super::schema::room;
#[derive(Insertable)]
#[table_name = "room"]
pub struct NewRoom<'a> {
    pub name: &'a str,
    pub description: &'a str,
}

// important: order has to be the same as sql
use super::schema::messages;
#[derive(Insertable)]
#[table_name = "messages"]
pub struct NewMessage<'a> {
    pub sender: i32,
    pub body: &'a str,
    pub sent_time: std::time::SystemTime,
    pub room_id: i32,
}

use super::schema::sub_user_room;
#[derive(Insertable)]
#[table_name = "sub_user_room"]
pub struct NewSubscription {
    pub room_id: i32,
    pub user_id: i32,
    // pub entrance_time: std::time::SystemTime, set by db
}

#[derive(Queryable, Debug)]
pub struct Subscription {
    pub roomid: i32,
    pub userid: i32,
    pub entrance_time: std::time::SystemTime,
}
