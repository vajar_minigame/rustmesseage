table! {
    messages (id) {
        id -> Int4,
        body -> Text,
        sender -> Int4,
        sent_time -> Timestamp,
        room_id -> Int4,
    }
}

table! {
    room (id) {
        id -> Int4,
        name -> Varchar,
        description -> Text,
        created -> Timestamp,
    }
}

table! {
    sub_user_room (room_id, user_id) {
        room_id -> Int4,
        user_id -> Int4,
        entrance_time -> Timestamp,
    }
}

joinable!(messages -> room (room_id));
joinable!(sub_user_room -> room (room_id));

allow_tables_to_appear_in_same_query!(
    messages,
    room,
    sub_user_room,
);
