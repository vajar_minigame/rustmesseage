pub mod db_models;
pub mod schema;
use log::warn;

use self::db_models::{Message, NewMessage, NewRoom, NewSubscription, Room, Subscription};
use diesel::pg::PgConnection;
use diesel::prelude::*;
use dotenv::dotenv;
use std::env;
use std::time::SystemTime;
extern crate r2d2;
extern crate r2d2_diesel;
use std::ops::Deref;

#[derive(Clone)]
pub struct PostgresConn {
    pub conn: r2d2::Pool<r2d2_diesel::ConnectionManager<PgConnection>>,
}

pub trait DBHandler {
    fn establish_connection() -> Self;
    fn create_room(&self, name: &str, description: &str) -> Option<Room>;
    fn create_message(
        &self,
        sender: i32,
        sent_time: SystemTime,
        room_id: i32,
        body: &str,
    ) -> Option<Message>;
    fn subscribe_user_room(&self, user_id: i32, room_id: i32) -> Option<Subscription>;
    fn get_all_rooms(&self) -> Option<Vec<Room>>;
    fn get_rooms_by_user(&self, user_id: i32) -> Option<Vec<Room>>;
    fn get_last_messages_by_room(&self, room_id: i32, limit: i32) -> Option<Vec<Message>>;
}
impl DBHandler for PostgresConn {
    fn establish_connection() -> PostgresConn {
        dotenv().ok();
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let manager = r2d2_diesel::ConnectionManager::<PgConnection>::new(database_url);
        let pool = r2d2::Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        PostgresConn { conn: pool }
    }

    fn create_room(&self, name: &str, description: &str) -> Option<Room> {
        use schema::room;

        let new_room = NewRoom {
            name: name,
            description: description,
        };

        let res = diesel::insert_into(room::table)
            .values(&new_room)
            .get_result(self.conn.get().unwrap().deref());

        match res {
            Err(e) => {
                println!("Error creating new room {:?}", e);
                return None;
            }
            Ok(v) => Some(v),
        }
    }

    fn create_message(
        &self,
        sender: i32,
        sent_time: SystemTime,
        room_id: i32,
        body: &str,
    ) -> Option<Message> {
        use schema::messages;

        let new_message = NewMessage {
            sender,
            sent_time,
            room_id,
            body,
        };

        let res = diesel::insert_into(messages::table)
            .values(&new_message)
            .get_result(self.conn.get().unwrap().deref());
        match res {
            Err(e) => {
                println!("Error saving new post {:?}", e);
                return None;
            }
            Ok(v) => Some(v),
        }
    }

    fn subscribe_user_room(&self, user_id: i32, room_id: i32) -> Option<Subscription> {
        use schema::sub_user_room;

        let new_sub = NewSubscription {
            room_id: room_id,
            user_id: user_id,
        };

        let res = diesel::insert_into(sub_user_room::table)
            .values(&new_sub)
            .get_result(self.conn.get().unwrap().deref());

        match res {
            Err(e) => {
                println!("error occured adding stuff {:?}", e);
                return None;
            }
            Ok(v) => Some(v),
        }
    }

    fn get_all_rooms(&self) -> Option<Vec<Room>> {
        use schema::room::dsl::room;

        let rooms = room
            .limit(100)
            .load::<Room>(self.conn.get().unwrap().deref());

        match rooms {
            Err(e) => {
                warn!("Error occured loading rooms {}", e);
                return None;
            }
            Ok(v) => Some(v),
        }
    }

    fn get_rooms_by_user(&self, user_id: i32) -> Option<Vec<Room>> {
        use schema::room::dsl::room;

        use schema::room as roominfo;

        let rooms = room
            .select(roominfo::all_columns)
            .limit(100)
            .inner_join(schema::sub_user_room::table)
            .filter(schema::sub_user_room::columns::user_id.eq(user_id))
            .load::<Room>(self.conn.get().unwrap().deref());

        match rooms {
            Err(e) => {
                warn!("Error occured loading rooms {}", e);
                return None;
            }
            Ok(v) => Some(v),
        }
    }

    fn get_last_messages_by_room(&self, room_id: i32, limit: i32) -> Option<Vec<Message>> {
        use schema::messages::dsl::messages;

        let msgs = messages
            .limit(limit as i64)
            .filter(schema::messages::columns::room_id.eq(room_id))
            .order(schema::messages::columns::sent_time.desc())
            .load::<Message>(self.conn.get().unwrap().deref());

        match msgs {
            Err(e) => {
                warn!("Error occured loading messages {}", e);
                return None;
            }
            Ok(v) => Some(v),
        }
    }
}
