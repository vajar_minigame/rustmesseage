#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Monster {
    #[prost(string, tag="1")]
    pub name: std::string::String,
    #[prost(int32, tag="2")]
    pub id: i32,
    #[prost(int32, tag="3")]
    pub player_id: i32,
    #[prost(message, optional, tag="4")]
    pub battle_values: ::std::option::Option<BattleValues>,
    #[prost(message, optional, tag="5")]
    pub body_values: ::std::option::Option<BodyValues>,
    #[prost(message, optional, tag="8")]
    pub last_update: ::std::option::Option<::prost_types::Timestamp>,
    #[prost(message, optional, tag="6")]
    pub activity: ::std::option::Option<Activity>,
    #[prost(message, optional, tag="7")]
    pub status: ::std::option::Option<StatusFlags>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct StatusFlags {
    #[prost(bool, tag="1")]
    pub is_alive: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleValues {
    #[prost(int32, tag="1")]
    pub attack: i32,
    #[prost(int32, tag="2")]
    pub defense: i32,
    #[prost(int32, tag="3")]
    pub max_hp: i32,
    #[prost(int32, tag="4")]
    pub remaining_hp: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BodyValues {
    #[prost(double, tag="1")]
    pub remaining_saturation: f64,
    #[prost(double, tag="2")]
    pub max_saturation: f64,
    #[prost(int32, tag="3")]
    pub mass: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Activity {
    #[prost(oneof="activity::Activity", tags="1, 2")]
    pub activity: ::std::option::Option<activity::Activity>,
}
pub mod activity {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Activity {
        #[prost(message, tag="1")]
        QuestId(super::super::common::QuestId),
        #[prost(message, tag="2")]
        BattleId(super::super::common::BattleId),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddMonCall {
    #[prost(message, optional, tag="1")]
    pub mon: ::std::option::Option<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetMonByIdCall {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetMonByUserIdCall {
    #[prost(message, optional, tag="1")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateMonCall {
    #[prost(message, optional, tag="1")]
    pub mon: ::std::option::Option<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DeleteMonCall {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LockRequest {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::MonId>,
    #[prost(message, optional, tag="2")]
    pub activity: ::std::option::Option<Activity>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterDeletedEvent {
    #[prost(message, optional, tag="1")]
    pub mon_id: ::std::option::Option<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterAddedEvent {
    #[prost(message, optional, tag="1")]
    pub mon: ::std::option::Option<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterUpdatedEvent {
    #[prost(message, optional, tag="1")]
    pub mon: ::std::option::Option<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterEvent {
    #[prost(oneof="monster_event::Event", tags="1, 2, 3")]
    pub event: ::std::option::Option<monster_event::Event>,
}
pub mod monster_event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag="1")]
        AddedEvent(super::MonsterAddedEvent),
        #[prost(message, tag="2")]
        DeleteEvent(super::MonsterDeletedEvent),
        #[prost(message, tag="3")]
        UpdateEvent(super::MonsterUpdatedEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonServiceCall {
    #[prost(message, optional, tag="6")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof="mon_service_call::Method", tags="1, 2, 3, 4, 5")]
    pub method: ::std::option::Option<mon_service_call::Method>,
}
pub mod mon_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Method {
        #[prost(message, tag="1")]
        AddMon(super::AddMonCall),
        #[prost(message, tag="2")]
        GetbyId(super::GetMonByIdCall),
        #[prost(message, tag="3")]
        GetbyuserId(super::GetMonByUserIdCall),
        #[prost(message, tag="4")]
        Update(super::UpdateMonCall),
        #[prost(message, tag="5")]
        Delete(super::DeleteMonCall),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonsterList {
    #[prost(message, repeated, tag="1")]
    pub monsters: ::std::vec::Vec<Monster>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonServiceResponse {
    #[prost(message, optional, tag="5")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof="mon_service_response::Data", tags="1, 2, 3, 4")]
    pub data: ::std::option::Option<mon_service_response::Data>,
}
pub mod mon_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag="1")]
        Id(super::super::common::MonId),
        #[prost(message, tag="2")]
        Mon(super::Monster),
        #[prost(message, tag="3")]
        Status(super::super::common::ResponseStatus),
        #[prost(message, tag="4")]
        Monsters(super::MonsterList),
    }
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{Monster, LockRequest, MonsterList};

    #[derive(Debug, Clone)]
    pub struct MonsterServices<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> MonsterServices<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn add_mon<R>(&mut self, request: grpc::Request<Monster>) -> grpc::unary::ResponseFuture<super::super::common::MonId, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<Monster>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/monster.MonsterServices/AddMon");
            self.inner.unary(request, path)
        }

        pub fn lock_monster<R>(&mut self, request: grpc::Request<LockRequest>) -> grpc::unary::ResponseFuture<Monster, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<LockRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/monster.MonsterServices/LockMonster");
            self.inner.unary(request, path)
        }

        pub fn get_mon_by_id<R>(&mut self, request: grpc::Request<super::super::common::MonId>) -> grpc::unary::ResponseFuture<Monster, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::MonId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/monster.MonsterServices/GetMonByID");
            self.inner.unary(request, path)
        }

        pub fn get_mons_by_user_id<R>(&mut self, request: grpc::Request<super::super::common::UserId>) -> grpc::unary::ResponseFuture<MonsterList, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::UserId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/monster.MonsterServices/GetMonsByUserID");
            self.inner.unary(request, path)
        }

        pub fn update_mon<R>(&mut self, request: grpc::Request<Monster>) -> grpc::unary::ResponseFuture<Monster, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<Monster>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/monster.MonsterServices/UpdateMon");
            self.inner.unary(request, path)
        }

        pub fn delete_mon<R>(&mut self, request: grpc::Request<super::super::common::MonId>) -> grpc::unary::ResponseFuture<super::super::common::ResponseStatus, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::MonId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/monster.MonsterServices/DeleteMon");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{Monster, LockRequest, MonsterList};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait MonsterServices: Clone {
        type AddMonFuture: futures::Future<Item = grpc::Response<super::super::common::MonId>, Error = grpc::Status>;
        type LockMonsterFuture: futures::Future<Item = grpc::Response<Monster>, Error = grpc::Status>;
        type GetMonByIdFuture: futures::Future<Item = grpc::Response<Monster>, Error = grpc::Status>;
        type GetMonsByUserIdFuture: futures::Future<Item = grpc::Response<MonsterList>, Error = grpc::Status>;
        type UpdateMonFuture: futures::Future<Item = grpc::Response<Monster>, Error = grpc::Status>;
        type DeleteMonFuture: futures::Future<Item = grpc::Response<super::super::common::ResponseStatus>, Error = grpc::Status>;

        fn add_mon(&mut self, request: grpc::Request<Monster>) -> Self::AddMonFuture;

        fn lock_monster(&mut self, request: grpc::Request<LockRequest>) -> Self::LockMonsterFuture;

        fn get_mon_by_id(&mut self, request: grpc::Request<super::super::common::MonId>) -> Self::GetMonByIdFuture;

        fn get_mons_by_user_id(&mut self, request: grpc::Request<super::super::common::UserId>) -> Self::GetMonsByUserIdFuture;

        fn update_mon(&mut self, request: grpc::Request<Monster>) -> Self::UpdateMonFuture;

        fn delete_mon(&mut self, request: grpc::Request<super::super::common::MonId>) -> Self::DeleteMonFuture;
    }

    #[derive(Debug, Clone)]
    pub struct MonsterServicesServer<T> {
        monster_services: T,
    }

    impl<T> MonsterServicesServer<T>
    where T: MonsterServices,
    {
        pub fn new(monster_services: T) -> Self {
            Self { monster_services }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for MonsterServicesServer<T>
    where T: MonsterServices,
    {
        type Response = http::Response<monster_services::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = monster_services::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::monster_services::Kind::*;

            match request.uri().path() {
                "/monster.MonsterServices/AddMon" => {
                    let service = monster_services::methods::AddMon(self.monster_services.clone());
                    let response = grpc::unary(service, request);
                    monster_services::ResponseFuture { kind: AddMon(response) }
                }
                "/monster.MonsterServices/LockMonster" => {
                    let service = monster_services::methods::LockMonster(self.monster_services.clone());
                    let response = grpc::unary(service, request);
                    monster_services::ResponseFuture { kind: LockMonster(response) }
                }
                "/monster.MonsterServices/GetMonByID" => {
                    let service = monster_services::methods::GetMonById(self.monster_services.clone());
                    let response = grpc::unary(service, request);
                    monster_services::ResponseFuture { kind: GetMonById(response) }
                }
                "/monster.MonsterServices/GetMonsByUserID" => {
                    let service = monster_services::methods::GetMonsByUserId(self.monster_services.clone());
                    let response = grpc::unary(service, request);
                    monster_services::ResponseFuture { kind: GetMonsByUserId(response) }
                }
                "/monster.MonsterServices/UpdateMon" => {
                    let service = monster_services::methods::UpdateMon(self.monster_services.clone());
                    let response = grpc::unary(service, request);
                    monster_services::ResponseFuture { kind: UpdateMon(response) }
                }
                "/monster.MonsterServices/DeleteMon" => {
                    let service = monster_services::methods::DeleteMon(self.monster_services.clone());
                    let response = grpc::unary(service, request);
                    monster_services::ResponseFuture { kind: DeleteMon(response) }
                }
                _ => {
                    monster_services::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for MonsterServicesServer<T>
    where T: MonsterServices,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for MonsterServicesServer<T>
    where T: MonsterServices,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod monster_services {
        use ::tower_grpc::codegen::server::*;
        use super::MonsterServices;
        use super::super::{Monster, LockRequest};

        pub struct ResponseFuture<T>
        where T: MonsterServices,
        {
            pub(super) kind: Kind<
                // AddMon
                grpc::unary::ResponseFuture<methods::AddMon<T>, grpc::BoxBody, Monster>,
                // LockMonster
                grpc::unary::ResponseFuture<methods::LockMonster<T>, grpc::BoxBody, LockRequest>,
                // GetMonByID
                grpc::unary::ResponseFuture<methods::GetMonById<T>, grpc::BoxBody, super::super::super::common::MonId>,
                // GetMonsByUserID
                grpc::unary::ResponseFuture<methods::GetMonsByUserId<T>, grpc::BoxBody, super::super::super::common::UserId>,
                // UpdateMon
                grpc::unary::ResponseFuture<methods::UpdateMon<T>, grpc::BoxBody, Monster>,
                // DeleteMon
                grpc::unary::ResponseFuture<methods::DeleteMon<T>, grpc::BoxBody, super::super::super::common::MonId>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: MonsterServices,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddMon(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: AddMon(body) }
                        });
                        Ok(response.into())
                    }
                    LockMonster(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: LockMonster(body) }
                        });
                        Ok(response.into())
                    }
                    GetMonById(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetMonById(body) }
                        });
                        Ok(response.into())
                    }
                    GetMonsByUserId(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetMonsByUserId(body) }
                        });
                        Ok(response.into())
                    }
                    UpdateMon(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: UpdateMon(body) }
                        });
                        Ok(response.into())
                    }
                    DeleteMon(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: DeleteMon(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: MonsterServices,
        {
            pub(super) kind: Kind<
                // AddMon
                grpc::Encode<grpc::unary::Once<<methods::AddMon<T> as grpc::UnaryService<Monster>>::Response>>,
                // LockMonster
                grpc::Encode<grpc::unary::Once<<methods::LockMonster<T> as grpc::UnaryService<LockRequest>>::Response>>,
                // GetMonByID
                grpc::Encode<grpc::unary::Once<<methods::GetMonById<T> as grpc::UnaryService<super::super::super::common::MonId>>::Response>>,
                // GetMonsByUserID
                grpc::Encode<grpc::unary::Once<<methods::GetMonsByUserId<T> as grpc::UnaryService<super::super::super::common::UserId>>::Response>>,
                // UpdateMon
                grpc::Encode<grpc::unary::Once<<methods::UpdateMon<T> as grpc::UnaryService<Monster>>::Response>>,
                // DeleteMon
                grpc::Encode<grpc::unary::Once<<methods::DeleteMon<T> as grpc::UnaryService<super::super::super::common::MonId>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: MonsterServices,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    AddMon(ref v) => v.is_end_stream(),
                    LockMonster(ref v) => v.is_end_stream(),
                    GetMonById(ref v) => v.is_end_stream(),
                    GetMonsByUserId(ref v) => v.is_end_stream(),
                    UpdateMon(ref v) => v.is_end_stream(),
                    DeleteMon(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddMon(ref mut v) => v.poll_data(),
                    LockMonster(ref mut v) => v.poll_data(),
                    GetMonById(ref mut v) => v.poll_data(),
                    GetMonsByUserId(ref mut v) => v.poll_data(),
                    UpdateMon(ref mut v) => v.poll_data(),
                    DeleteMon(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddMon(ref mut v) => v.poll_trailers(),
                    LockMonster(ref mut v) => v.poll_trailers(),
                    GetMonById(ref mut v) => v.poll_trailers(),
                    GetMonsByUserId(ref mut v) => v.poll_trailers(),
                    UpdateMon(ref mut v) => v.poll_trailers(),
                    DeleteMon(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<AddMon, LockMonster, GetMonById, GetMonsByUserId, UpdateMon, DeleteMon, __Generated__Unimplemented> {
            AddMon(AddMon),
            LockMonster(LockMonster),
            GetMonById(GetMonById),
            GetMonsByUserId(GetMonsByUserId),
            UpdateMon(UpdateMon),
            DeleteMon(DeleteMon),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{MonsterServices, Monster, LockRequest, MonsterList};

            pub struct AddMon<T>(pub T);

            impl<T> tower::Service<grpc::Request<Monster>> for AddMon<T>
            where T: MonsterServices,
            {
                type Response = grpc::Response<super::super::super::super::common::MonId>;
                type Error = grpc::Status;
                type Future = T::AddMonFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<Monster>) -> Self::Future {
                    self.0.add_mon(request)
                }
            }

            pub struct LockMonster<T>(pub T);

            impl<T> tower::Service<grpc::Request<LockRequest>> for LockMonster<T>
            where T: MonsterServices,
            {
                type Response = grpc::Response<Monster>;
                type Error = grpc::Status;
                type Future = T::LockMonsterFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<LockRequest>) -> Self::Future {
                    self.0.lock_monster(request)
                }
            }

            pub struct GetMonById<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::MonId>> for GetMonById<T>
            where T: MonsterServices,
            {
                type Response = grpc::Response<Monster>;
                type Error = grpc::Status;
                type Future = T::GetMonByIdFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::MonId>) -> Self::Future {
                    self.0.get_mon_by_id(request)
                }
            }

            pub struct GetMonsByUserId<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::UserId>> for GetMonsByUserId<T>
            where T: MonsterServices,
            {
                type Response = grpc::Response<MonsterList>;
                type Error = grpc::Status;
                type Future = T::GetMonsByUserIdFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::UserId>) -> Self::Future {
                    self.0.get_mons_by_user_id(request)
                }
            }

            pub struct UpdateMon<T>(pub T);

            impl<T> tower::Service<grpc::Request<Monster>> for UpdateMon<T>
            where T: MonsterServices,
            {
                type Response = grpc::Response<Monster>;
                type Error = grpc::Status;
                type Future = T::UpdateMonFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<Monster>) -> Self::Future {
                    self.0.update_mon(request)
                }
            }

            pub struct DeleteMon<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::MonId>> for DeleteMon<T>
            where T: MonsterServices,
            {
                type Response = grpc::Response<super::super::super::super::common::ResponseStatus>;
                type Error = grpc::Status;
                type Future = T::DeleteMonFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::MonId>) -> Self::Future {
                    self.0.delete_mon(request)
                }
            }
        }
    }
}
