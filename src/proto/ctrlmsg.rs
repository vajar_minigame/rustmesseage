#[derive(Clone, PartialEq, ::prost::Message)]
pub struct FuncCall {
    #[prost(oneof="func_call::Message", tags="1, 2, 3, 4, 5, 6")]
    pub message: ::std::option::Option<func_call::Message>,
}
pub mod func_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Message {
        #[prost(message, tag="1")]
        AuthServiceCall(super::super::auth::AuthServiceCall),
        #[prost(message, tag="2")]
        MonServiceCall(super::super::monster::MonServiceCall),
        #[prost(message, tag="3")]
        UserServiceCall(super::super::user::UserServiceCall),
        #[prost(message, tag="4")]
        InvServiceCall(super::super::item::InventoryServiceCall),
        #[prost(message, tag="5")]
        InteractServiceCall(super::super::interaction::InteractionServiceCall),
        #[prost(message, tag="6")]
        QuestServiceCall(super::super::quest::QuestServiceCall),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Message {
    #[prost(oneof="message::Msg", tags="1, 2, 3")]
    pub msg: ::std::option::Option<message::Msg>,
}
pub mod message {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Msg {
        #[prost(message, tag="1")]
        Call(super::FuncCall),
        #[prost(message, tag="2")]
        Response(super::FuncResponse),
        #[prost(message, tag="3")]
        Event(super::Event),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct FuncResponse {
    #[prost(oneof="func_response::Message", tags="1, 2, 3, 4, 5, 6")]
    pub message: ::std::option::Option<func_response::Message>,
}
pub mod func_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Message {
        #[prost(message, tag="1")]
        MonServiceRes(super::super::monster::MonServiceResponse),
        #[prost(message, tag="2")]
        AuthServiceRes(super::super::auth::AuthServiceResponse),
        #[prost(message, tag="3")]
        UserServiceRes(super::super::user::UserServiceResponse),
        #[prost(message, tag="4")]
        InvServiceRes(super::super::item::InventoryServiceResponse),
        #[prost(message, tag="5")]
        InteractServiceRes(super::super::interaction::InteractionServiceResponse),
        #[prost(message, tag="6")]
        QuestServiceRes(super::super::quest::QuestServiceResponse),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Event {
    #[prost(oneof="event::Event", tags="1, 2, 3, 4")]
    pub event: ::std::option::Option<event::Event>,
}
pub mod event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag="1")]
        MonEvent(super::super::monster::MonsterEvent),
        #[prost(message, tag="2")]
        ItemEvent(super::super::item::ItemEvent),
        #[prost(message, tag="3")]
        QuestEvent(super::super::quest::QuestEvent),
        #[prost(message, tag="4")]
        BattleEvent(super::super::battle::BattleEvent),
    }
}
