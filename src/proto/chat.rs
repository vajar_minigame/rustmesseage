#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Message {
    #[prost(message, optional, tag="1")]
    pub sender: ::std::option::Option<super::common::UserId>,
    #[prost(message, optional, tag="2")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(string, tag="3")]
    pub body: std::string::String,
    #[prost(message, optional, tag="4")]
    pub sent_time: ::std::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RoomId {
    #[prost(int32, tag="1")]
    pub id: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct SendResponse {
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LeaveResponse {
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct JoinResponse {
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct JoinEvent {
    #[prost(message, optional, tag="1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(message, optional, tag="2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LeaveEvent {
    #[prost(message, optional, tag="1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(message, optional, tag="2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct JoinRequest {
    #[prost(message, optional, tag="1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(message, optional, tag="2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LeaveRequest {
    #[prost(message, optional, tag="1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(message, optional, tag="2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LastMessagesRequest {
    #[prost(message, optional, tag="1")]
    pub room_id: ::std::option::Option<RoomId>,
    #[prost(int32, tag="2")]
    pub count: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Rooms {
    #[prost(message, repeated, tag="1")]
    pub rooms: ::std::vec::Vec<RoomId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Event {
    #[prost(oneof="event::Event", tags="1, 2, 3")]
    pub event: ::std::option::Option<event::Event>,
}
pub mod event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag="1")]
        Login(super::JoinEvent),
        #[prost(message, tag="2")]
        Logout(super::LeaveEvent),
        #[prost(message, tag="3")]
        Msg(super::Message),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RoomRequest {
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Messages {
    #[prost(message, repeated, tag="1")]
    pub messages: ::std::vec::Vec<Message>,
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{Message, SendResponse, Event, JoinRequest, JoinResponse, LeaveRequest, LeaveResponse, Rooms, RoomRequest, LastMessagesRequest, Messages};

    #[derive(Debug, Clone)]
    pub struct ChatService<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> ChatService<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn send_message<R>(&mut self, request: grpc::Request<Message>) -> grpc::unary::ResponseFuture<SendResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<Message>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/chat.ChatService/SendMessage");
            self.inner.unary(request, path)
        }

        pub fn get_event_stream<R>(&mut self, request: grpc::Request<super::super::common::UserId>) -> grpc::server_streaming::ResponseFuture<Event, T::Future>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::UserId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/chat.ChatService/getEventStream");
            self.inner.server_streaming(request, path)
        }

        pub fn join_room<R>(&mut self, request: grpc::Request<JoinRequest>) -> grpc::unary::ResponseFuture<JoinResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<JoinRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/chat.ChatService/JoinRoom");
            self.inner.unary(request, path)
        }

        pub fn leave_room<R>(&mut self, request: grpc::Request<LeaveRequest>) -> grpc::unary::ResponseFuture<LeaveResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<LeaveRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/chat.ChatService/LeaveRoom");
            self.inner.unary(request, path)
        }

        pub fn get_rooms_by_user<R>(&mut self, request: grpc::Request<super::super::common::UserId>) -> grpc::unary::ResponseFuture<Rooms, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::UserId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/chat.ChatService/GetRoomsByUser");
            self.inner.unary(request, path)
        }

        pub fn get_all_rooms<R>(&mut self, request: grpc::Request<RoomRequest>) -> grpc::unary::ResponseFuture<Rooms, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<RoomRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/chat.ChatService/getAllRooms");
            self.inner.unary(request, path)
        }

        pub fn get_last_messages_by_room<R>(&mut self, request: grpc::Request<LastMessagesRequest>) -> grpc::unary::ResponseFuture<Messages, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<LastMessagesRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/chat.ChatService/GetLastMessagesByRoom");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{Message, SendResponse, Event, JoinRequest, JoinResponse, LeaveRequest, LeaveResponse, Rooms, RoomRequest, LastMessagesRequest, Messages};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait ChatService: Clone {
        type SendMessageFuture: futures::Future<Item = grpc::Response<SendResponse>, Error = grpc::Status>;
        type GetEventStreamStream: futures::Stream<Item = Event, Error = grpc::Status>;
        type GetEventStreamFuture: futures::Future<Item = grpc::Response<Self::GetEventStreamStream>, Error = grpc::Status>;
        type JoinRoomFuture: futures::Future<Item = grpc::Response<JoinResponse>, Error = grpc::Status>;
        type LeaveRoomFuture: futures::Future<Item = grpc::Response<LeaveResponse>, Error = grpc::Status>;
        type GetRoomsByUserFuture: futures::Future<Item = grpc::Response<Rooms>, Error = grpc::Status>;
        type GetAllRoomsFuture: futures::Future<Item = grpc::Response<Rooms>, Error = grpc::Status>;
        type GetLastMessagesByRoomFuture: futures::Future<Item = grpc::Response<Messages>, Error = grpc::Status>;

        /// Sends a message
        fn send_message(&mut self, request: grpc::Request<Message>) -> Self::SendMessageFuture;

        fn get_event_stream(&mut self, request: grpc::Request<super::super::common::UserId>) -> Self::GetEventStreamFuture;

        fn join_room(&mut self, request: grpc::Request<JoinRequest>) -> Self::JoinRoomFuture;

        fn leave_room(&mut self, request: grpc::Request<LeaveRequest>) -> Self::LeaveRoomFuture;

        fn get_rooms_by_user(&mut self, request: grpc::Request<super::super::common::UserId>) -> Self::GetRoomsByUserFuture;

        fn get_all_rooms(&mut self, request: grpc::Request<RoomRequest>) -> Self::GetAllRoomsFuture;

        fn get_last_messages_by_room(&mut self, request: grpc::Request<LastMessagesRequest>) -> Self::GetLastMessagesByRoomFuture;
    }

    #[derive(Debug, Clone)]
    pub struct ChatServiceServer<T> {
        chat_service: T,
    }

    impl<T> ChatServiceServer<T>
    where T: ChatService,
    {
        pub fn new(chat_service: T) -> Self {
            Self { chat_service }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for ChatServiceServer<T>
    where T: ChatService,
    {
        type Response = http::Response<chat_service::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = chat_service::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::chat_service::Kind::*;

            match request.uri().path() {
                "/chat.ChatService/SendMessage" => {
                    let service = chat_service::methods::SendMessage(self.chat_service.clone());
                    let response = grpc::unary(service, request);
                    chat_service::ResponseFuture { kind: SendMessage(response) }
                }
                "/chat.ChatService/getEventStream" => {
                    let service = chat_service::methods::GetEventStream(self.chat_service.clone());
                    let response = grpc::server_streaming(service, request);
                    chat_service::ResponseFuture { kind: GetEventStream(response) }
                }
                "/chat.ChatService/JoinRoom" => {
                    let service = chat_service::methods::JoinRoom(self.chat_service.clone());
                    let response = grpc::unary(service, request);
                    chat_service::ResponseFuture { kind: JoinRoom(response) }
                }
                "/chat.ChatService/LeaveRoom" => {
                    let service = chat_service::methods::LeaveRoom(self.chat_service.clone());
                    let response = grpc::unary(service, request);
                    chat_service::ResponseFuture { kind: LeaveRoom(response) }
                }
                "/chat.ChatService/GetRoomsByUser" => {
                    let service = chat_service::methods::GetRoomsByUser(self.chat_service.clone());
                    let response = grpc::unary(service, request);
                    chat_service::ResponseFuture { kind: GetRoomsByUser(response) }
                }
                "/chat.ChatService/getAllRooms" => {
                    let service = chat_service::methods::GetAllRooms(self.chat_service.clone());
                    let response = grpc::unary(service, request);
                    chat_service::ResponseFuture { kind: GetAllRooms(response) }
                }
                "/chat.ChatService/GetLastMessagesByRoom" => {
                    let service = chat_service::methods::GetLastMessagesByRoom(self.chat_service.clone());
                    let response = grpc::unary(service, request);
                    chat_service::ResponseFuture { kind: GetLastMessagesByRoom(response) }
                }
                _ => {
                    chat_service::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for ChatServiceServer<T>
    where T: ChatService,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for ChatServiceServer<T>
    where T: ChatService,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod chat_service {
        use ::tower_grpc::codegen::server::*;
        use super::ChatService;
        use super::super::{Message, JoinRequest, LeaveRequest, RoomRequest, LastMessagesRequest};

        pub struct ResponseFuture<T>
        where T: ChatService,
        {
            pub(super) kind: Kind<
                // SendMessage
                grpc::unary::ResponseFuture<methods::SendMessage<T>, grpc::BoxBody, Message>,
                // getEventStream
                grpc::server_streaming::ResponseFuture<methods::GetEventStream<T>, grpc::BoxBody, super::super::super::common::UserId>,
                // JoinRoom
                grpc::unary::ResponseFuture<methods::JoinRoom<T>, grpc::BoxBody, JoinRequest>,
                // LeaveRoom
                grpc::unary::ResponseFuture<methods::LeaveRoom<T>, grpc::BoxBody, LeaveRequest>,
                // GetRoomsByUser
                grpc::unary::ResponseFuture<methods::GetRoomsByUser<T>, grpc::BoxBody, super::super::super::common::UserId>,
                // getAllRooms
                grpc::unary::ResponseFuture<methods::GetAllRooms<T>, grpc::BoxBody, RoomRequest>,
                // GetLastMessagesByRoom
                grpc::unary::ResponseFuture<methods::GetLastMessagesByRoom<T>, grpc::BoxBody, LastMessagesRequest>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: ChatService,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    SendMessage(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: SendMessage(body) }
                        });
                        Ok(response.into())
                    }
                    GetEventStream(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetEventStream(body) }
                        });
                        Ok(response.into())
                    }
                    JoinRoom(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: JoinRoom(body) }
                        });
                        Ok(response.into())
                    }
                    LeaveRoom(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: LeaveRoom(body) }
                        });
                        Ok(response.into())
                    }
                    GetRoomsByUser(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetRoomsByUser(body) }
                        });
                        Ok(response.into())
                    }
                    GetAllRooms(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetAllRooms(body) }
                        });
                        Ok(response.into())
                    }
                    GetLastMessagesByRoom(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetLastMessagesByRoom(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: ChatService,
        {
            pub(super) kind: Kind<
                // SendMessage
                grpc::Encode<grpc::unary::Once<<methods::SendMessage<T> as grpc::UnaryService<Message>>::Response>>,
                // getEventStream
                grpc::Encode<<methods::GetEventStream<T> as grpc::ServerStreamingService<super::super::super::common::UserId>>::ResponseStream>,
                // JoinRoom
                grpc::Encode<grpc::unary::Once<<methods::JoinRoom<T> as grpc::UnaryService<JoinRequest>>::Response>>,
                // LeaveRoom
                grpc::Encode<grpc::unary::Once<<methods::LeaveRoom<T> as grpc::UnaryService<LeaveRequest>>::Response>>,
                // GetRoomsByUser
                grpc::Encode<grpc::unary::Once<<methods::GetRoomsByUser<T> as grpc::UnaryService<super::super::super::common::UserId>>::Response>>,
                // getAllRooms
                grpc::Encode<grpc::unary::Once<<methods::GetAllRooms<T> as grpc::UnaryService<RoomRequest>>::Response>>,
                // GetLastMessagesByRoom
                grpc::Encode<grpc::unary::Once<<methods::GetLastMessagesByRoom<T> as grpc::UnaryService<LastMessagesRequest>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: ChatService,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    SendMessage(ref v) => v.is_end_stream(),
                    GetEventStream(ref v) => v.is_end_stream(),
                    JoinRoom(ref v) => v.is_end_stream(),
                    LeaveRoom(ref v) => v.is_end_stream(),
                    GetRoomsByUser(ref v) => v.is_end_stream(),
                    GetAllRooms(ref v) => v.is_end_stream(),
                    GetLastMessagesByRoom(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    SendMessage(ref mut v) => v.poll_data(),
                    GetEventStream(ref mut v) => v.poll_data(),
                    JoinRoom(ref mut v) => v.poll_data(),
                    LeaveRoom(ref mut v) => v.poll_data(),
                    GetRoomsByUser(ref mut v) => v.poll_data(),
                    GetAllRooms(ref mut v) => v.poll_data(),
                    GetLastMessagesByRoom(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    SendMessage(ref mut v) => v.poll_trailers(),
                    GetEventStream(ref mut v) => v.poll_trailers(),
                    JoinRoom(ref mut v) => v.poll_trailers(),
                    LeaveRoom(ref mut v) => v.poll_trailers(),
                    GetRoomsByUser(ref mut v) => v.poll_trailers(),
                    GetAllRooms(ref mut v) => v.poll_trailers(),
                    GetLastMessagesByRoom(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<SendMessage, GetEventStream, JoinRoom, LeaveRoom, GetRoomsByUser, GetAllRooms, GetLastMessagesByRoom, __Generated__Unimplemented> {
            SendMessage(SendMessage),
            GetEventStream(GetEventStream),
            JoinRoom(JoinRoom),
            LeaveRoom(LeaveRoom),
            GetRoomsByUser(GetRoomsByUser),
            GetAllRooms(GetAllRooms),
            GetLastMessagesByRoom(GetLastMessagesByRoom),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{ChatService, Message, SendResponse, JoinRequest, JoinResponse, LeaveRequest, LeaveResponse, Rooms, RoomRequest, LastMessagesRequest, Messages};

            pub struct SendMessage<T>(pub T);

            impl<T> tower::Service<grpc::Request<Message>> for SendMessage<T>
            where T: ChatService,
            {
                type Response = grpc::Response<SendResponse>;
                type Error = grpc::Status;
                type Future = T::SendMessageFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<Message>) -> Self::Future {
                    self.0.send_message(request)
                }
            }

            pub struct GetEventStream<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::UserId>> for GetEventStream<T>
            where T: ChatService,
            {
                type Response = grpc::Response<T::GetEventStreamStream>;
                type Error = grpc::Status;
                type Future = T::GetEventStreamFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::UserId>) -> Self::Future {
                    self.0.get_event_stream(request)
                }
            }

            pub struct JoinRoom<T>(pub T);

            impl<T> tower::Service<grpc::Request<JoinRequest>> for JoinRoom<T>
            where T: ChatService,
            {
                type Response = grpc::Response<JoinResponse>;
                type Error = grpc::Status;
                type Future = T::JoinRoomFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<JoinRequest>) -> Self::Future {
                    self.0.join_room(request)
                }
            }

            pub struct LeaveRoom<T>(pub T);

            impl<T> tower::Service<grpc::Request<LeaveRequest>> for LeaveRoom<T>
            where T: ChatService,
            {
                type Response = grpc::Response<LeaveResponse>;
                type Error = grpc::Status;
                type Future = T::LeaveRoomFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<LeaveRequest>) -> Self::Future {
                    self.0.leave_room(request)
                }
            }

            pub struct GetRoomsByUser<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::UserId>> for GetRoomsByUser<T>
            where T: ChatService,
            {
                type Response = grpc::Response<Rooms>;
                type Error = grpc::Status;
                type Future = T::GetRoomsByUserFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::UserId>) -> Self::Future {
                    self.0.get_rooms_by_user(request)
                }
            }

            pub struct GetAllRooms<T>(pub T);

            impl<T> tower::Service<grpc::Request<RoomRequest>> for GetAllRooms<T>
            where T: ChatService,
            {
                type Response = grpc::Response<Rooms>;
                type Error = grpc::Status;
                type Future = T::GetAllRoomsFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<RoomRequest>) -> Self::Future {
                    self.0.get_all_rooms(request)
                }
            }

            pub struct GetLastMessagesByRoom<T>(pub T);

            impl<T> tower::Service<grpc::Request<LastMessagesRequest>> for GetLastMessagesByRoom<T>
            where T: ChatService,
            {
                type Response = grpc::Response<Messages>;
                type Error = grpc::Status;
                type Future = T::GetLastMessagesByRoomFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<LastMessagesRequest>) -> Self::Future {
                    self.0.get_last_messages_by_room(request)
                }
            }
        }
    }
}
