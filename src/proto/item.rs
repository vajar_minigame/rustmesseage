#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Inventory {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::UserId>,
    #[prost(message, repeated, tag="2")]
    pub items: ::std::vec::Vec<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Item {
    #[prost(int32, tag="1")]
    pub id: i32,
    #[prost(string, tag="2")]
    pub r#type: std::string::String,
    #[prost(string, repeated, tag="3")]
    pub tags: ::std::vec::Vec<std::string::String>,
    #[prost(oneof="item::ItemContent", tags="4")]
    pub item_content: ::std::option::Option<item::ItemContent>,
}
pub mod item {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum ItemContent {
        #[prost(message, tag="4")]
        Food(super::Food),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Effect {
    #[prost(oneof="effect::Effect", tags="1, 2")]
    pub effect: ::std::option::Option<effect::Effect>,
}
pub mod effect {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Effect {
        #[prost(message, tag="1")]
        Buff(super::Buff),
        #[prost(message, tag="2")]
        Rec(super::Recovery),
    }
}
/// effect types
/// temporary increase of sth.
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Buff {
    #[prost(string, tag="1")]
    pub r#type: std::string::String,
    #[prost(float, tag="2")]
    pub value: f32,
}
/// recovery of resource
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Recovery {
    #[prost(string, tag="1")]
    pub r#type: std::string::String,
    #[prost(float, tag="2")]
    pub value: f32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Food {
    #[prost(message, repeated, tag="1")]
    pub effects: ::std::vec::Vec<Effect>,
}
//////////////////////////////////////////////////////////////////

#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddItem {
    #[prost(message, optional, tag="1")]
    pub item: ::std::option::Option<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetItem {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::ItemId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetInventory {
    #[prost(message, optional, tag="1")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateItem {
    #[prost(message, optional, tag="1")]
    pub item: ::std::option::Option<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DeleteItem {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::ItemId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemDeletedEvent {
    #[prost(message, optional, tag="1")]
    pub item_id: ::std::option::Option<super::common::ItemId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemAddedEvent {
    #[prost(message, optional, tag="1")]
    pub item: ::std::option::Option<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemUpdatedEvent {
    #[prost(message, optional, tag="1")]
    pub item: ::std::option::Option<Item>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemEvent {
    #[prost(oneof="item_event::Event", tags="1, 2, 3")]
    pub event: ::std::option::Option<item_event::Event>,
}
pub mod item_event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag="1")]
        AddedEvent(super::ItemAddedEvent),
        #[prost(message, tag="2")]
        DeleteEvent(super::ItemDeletedEvent),
        #[prost(message, tag="3")]
        UpdateEvent(super::ItemUpdatedEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct InventoryServiceCall {
    #[prost(message, optional, tag="6")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof="inventory_service_call::Method", tags="1, 2, 3, 4, 5")]
    pub method: ::std::option::Option<inventory_service_call::Method>,
}
pub mod inventory_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Method {
        #[prost(message, tag="1")]
        AddItem(super::AddItem),
        #[prost(message, tag="2")]
        GetItem(super::GetItem),
        #[prost(message, tag="3")]
        GetInventory(super::GetInventory),
        #[prost(message, tag="4")]
        UpdateItem(super::UpdateItem),
        #[prost(message, tag="5")]
        DeleteItem(super::DeleteItem),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct InventoryServiceResponse {
    #[prost(message, optional, tag="5")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof="inventory_service_response::Data", tags="1, 2, 3, 4")]
    pub data: ::std::option::Option<inventory_service_response::Data>,
}
pub mod inventory_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag="1")]
        ItemId(super::super::common::ItemId),
        #[prost(message, tag="2")]
        Item(super::Item),
        #[prost(message, tag="3")]
        Status(super::super::common::ResponseStatus),
        #[prost(message, tag="4")]
        Inv(super::Inventory),
    }
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{Item, Inventory};

    #[derive(Debug, Clone)]
    pub struct ItemService<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> ItemService<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn add_item<R>(&mut self, request: grpc::Request<Item>) -> grpc::unary::ResponseFuture<super::super::common::ItemId, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<Item>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/item.ItemService/AddItem");
            self.inner.unary(request, path)
        }

        pub fn get_item<R>(&mut self, request: grpc::Request<super::super::common::ItemId>) -> grpc::unary::ResponseFuture<Item, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::ItemId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/item.ItemService/GetItem");
            self.inner.unary(request, path)
        }

        pub fn get_inventory<R>(&mut self, request: grpc::Request<super::super::common::UserId>) -> grpc::unary::ResponseFuture<Inventory, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::UserId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/item.ItemService/GetInventory");
            self.inner.unary(request, path)
        }

        pub fn update_item<R>(&mut self, request: grpc::Request<Item>) -> grpc::unary::ResponseFuture<super::super::common::ResponseStatus, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<Item>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/item.ItemService/UpdateItem");
            self.inner.unary(request, path)
        }

        pub fn delete_item<R>(&mut self, request: grpc::Request<super::super::common::ItemId>) -> grpc::unary::ResponseFuture<super::super::common::ResponseStatus, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::ItemId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/item.ItemService/DeleteItem");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{Item, Inventory};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait ItemService: Clone {
        type AddItemFuture: futures::Future<Item = grpc::Response<super::super::common::ItemId>, Error = grpc::Status>;
        type GetItemFuture: futures::Future<Item = grpc::Response<Item>, Error = grpc::Status>;
        type GetInventoryFuture: futures::Future<Item = grpc::Response<Inventory>, Error = grpc::Status>;
        type UpdateItemFuture: futures::Future<Item = grpc::Response<super::super::common::ResponseStatus>, Error = grpc::Status>;
        type DeleteItemFuture: futures::Future<Item = grpc::Response<super::super::common::ResponseStatus>, Error = grpc::Status>;

        fn add_item(&mut self, request: grpc::Request<Item>) -> Self::AddItemFuture;

        fn get_item(&mut self, request: grpc::Request<super::super::common::ItemId>) -> Self::GetItemFuture;

        fn get_inventory(&mut self, request: grpc::Request<super::super::common::UserId>) -> Self::GetInventoryFuture;

        fn update_item(&mut self, request: grpc::Request<Item>) -> Self::UpdateItemFuture;

        fn delete_item(&mut self, request: grpc::Request<super::super::common::ItemId>) -> Self::DeleteItemFuture;
    }

    #[derive(Debug, Clone)]
    pub struct ItemServiceServer<T> {
        item_service: T,
    }

    impl<T> ItemServiceServer<T>
    where T: ItemService,
    {
        pub fn new(item_service: T) -> Self {
            Self { item_service }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for ItemServiceServer<T>
    where T: ItemService,
    {
        type Response = http::Response<item_service::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = item_service::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::item_service::Kind::*;

            match request.uri().path() {
                "/item.ItemService/AddItem" => {
                    let service = item_service::methods::AddItem(self.item_service.clone());
                    let response = grpc::unary(service, request);
                    item_service::ResponseFuture { kind: AddItem(response) }
                }
                "/item.ItemService/GetItem" => {
                    let service = item_service::methods::GetItem(self.item_service.clone());
                    let response = grpc::unary(service, request);
                    item_service::ResponseFuture { kind: GetItem(response) }
                }
                "/item.ItemService/GetInventory" => {
                    let service = item_service::methods::GetInventory(self.item_service.clone());
                    let response = grpc::unary(service, request);
                    item_service::ResponseFuture { kind: GetInventory(response) }
                }
                "/item.ItemService/UpdateItem" => {
                    let service = item_service::methods::UpdateItem(self.item_service.clone());
                    let response = grpc::unary(service, request);
                    item_service::ResponseFuture { kind: UpdateItem(response) }
                }
                "/item.ItemService/DeleteItem" => {
                    let service = item_service::methods::DeleteItem(self.item_service.clone());
                    let response = grpc::unary(service, request);
                    item_service::ResponseFuture { kind: DeleteItem(response) }
                }
                _ => {
                    item_service::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for ItemServiceServer<T>
    where T: ItemService,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for ItemServiceServer<T>
    where T: ItemService,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod item_service {
        use ::tower_grpc::codegen::server::*;
        use super::ItemService;
        use super::super::Item;

        pub struct ResponseFuture<T>
        where T: ItemService,
        {
            pub(super) kind: Kind<
                // AddItem
                grpc::unary::ResponseFuture<methods::AddItem<T>, grpc::BoxBody, Item>,
                // GetItem
                grpc::unary::ResponseFuture<methods::GetItem<T>, grpc::BoxBody, super::super::super::common::ItemId>,
                // GetInventory
                grpc::unary::ResponseFuture<methods::GetInventory<T>, grpc::BoxBody, super::super::super::common::UserId>,
                // UpdateItem
                grpc::unary::ResponseFuture<methods::UpdateItem<T>, grpc::BoxBody, Item>,
                // DeleteItem
                grpc::unary::ResponseFuture<methods::DeleteItem<T>, grpc::BoxBody, super::super::super::common::ItemId>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: ItemService,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddItem(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: AddItem(body) }
                        });
                        Ok(response.into())
                    }
                    GetItem(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetItem(body) }
                        });
                        Ok(response.into())
                    }
                    GetInventory(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetInventory(body) }
                        });
                        Ok(response.into())
                    }
                    UpdateItem(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: UpdateItem(body) }
                        });
                        Ok(response.into())
                    }
                    DeleteItem(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: DeleteItem(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: ItemService,
        {
            pub(super) kind: Kind<
                // AddItem
                grpc::Encode<grpc::unary::Once<<methods::AddItem<T> as grpc::UnaryService<Item>>::Response>>,
                // GetItem
                grpc::Encode<grpc::unary::Once<<methods::GetItem<T> as grpc::UnaryService<super::super::super::common::ItemId>>::Response>>,
                // GetInventory
                grpc::Encode<grpc::unary::Once<<methods::GetInventory<T> as grpc::UnaryService<super::super::super::common::UserId>>::Response>>,
                // UpdateItem
                grpc::Encode<grpc::unary::Once<<methods::UpdateItem<T> as grpc::UnaryService<Item>>::Response>>,
                // DeleteItem
                grpc::Encode<grpc::unary::Once<<methods::DeleteItem<T> as grpc::UnaryService<super::super::super::common::ItemId>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: ItemService,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    AddItem(ref v) => v.is_end_stream(),
                    GetItem(ref v) => v.is_end_stream(),
                    GetInventory(ref v) => v.is_end_stream(),
                    UpdateItem(ref v) => v.is_end_stream(),
                    DeleteItem(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddItem(ref mut v) => v.poll_data(),
                    GetItem(ref mut v) => v.poll_data(),
                    GetInventory(ref mut v) => v.poll_data(),
                    UpdateItem(ref mut v) => v.poll_data(),
                    DeleteItem(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddItem(ref mut v) => v.poll_trailers(),
                    GetItem(ref mut v) => v.poll_trailers(),
                    GetInventory(ref mut v) => v.poll_trailers(),
                    UpdateItem(ref mut v) => v.poll_trailers(),
                    DeleteItem(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<AddItem, GetItem, GetInventory, UpdateItem, DeleteItem, __Generated__Unimplemented> {
            AddItem(AddItem),
            GetItem(GetItem),
            GetInventory(GetInventory),
            UpdateItem(UpdateItem),
            DeleteItem(DeleteItem),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{ItemService, Item, Inventory};

            pub struct AddItem<T>(pub T);

            impl<T> tower::Service<grpc::Request<Item>> for AddItem<T>
            where T: ItemService,
            {
                type Response = grpc::Response<super::super::super::super::common::ItemId>;
                type Error = grpc::Status;
                type Future = T::AddItemFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<Item>) -> Self::Future {
                    self.0.add_item(request)
                }
            }

            pub struct GetItem<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::ItemId>> for GetItem<T>
            where T: ItemService,
            {
                type Response = grpc::Response<Item>;
                type Error = grpc::Status;
                type Future = T::GetItemFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::ItemId>) -> Self::Future {
                    self.0.get_item(request)
                }
            }

            pub struct GetInventory<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::UserId>> for GetInventory<T>
            where T: ItemService,
            {
                type Response = grpc::Response<Inventory>;
                type Error = grpc::Status;
                type Future = T::GetInventoryFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::UserId>) -> Self::Future {
                    self.0.get_inventory(request)
                }
            }

            pub struct UpdateItem<T>(pub T);

            impl<T> tower::Service<grpc::Request<Item>> for UpdateItem<T>
            where T: ItemService,
            {
                type Response = grpc::Response<super::super::super::super::common::ResponseStatus>;
                type Error = grpc::Status;
                type Future = T::UpdateItemFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<Item>) -> Self::Future {
                    self.0.update_item(request)
                }
            }

            pub struct DeleteItem<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::ItemId>> for DeleteItem<T>
            where T: ItemService,
            {
                type Response = grpc::Response<super::super::super::super::common::ResponseStatus>;
                type Error = grpc::Status;
                type Future = T::DeleteItemFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::ItemId>) -> Self::Future {
                    self.0.delete_item(request)
                }
            }
        }
    }
}
