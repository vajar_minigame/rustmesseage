#[derive(Clone, PartialEq, ::prost::Message)]
pub struct User {
    #[prost(int32, tag="1")]
    pub id: i32,
    #[prost(string, tag="2")]
    pub username: std::string::String,
    #[prost(message, optional, tag="3")]
    pub last_update: ::std::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetUserCall {
    #[prost(message, optional, tag="1")]
    pub user: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetUserResponse {
    #[prost(message, optional, tag="1")]
    pub user: ::std::option::Option<User>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserServiceCall {
    #[prost(oneof="user_service_call::Message", tags="1")]
    pub message: ::std::option::Option<user_service_call::Message>,
}
pub mod user_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Message {
        #[prost(message, tag="1")]
        GetUserCall(super::GetUserCall),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserServiceResponse {
    #[prost(oneof="user_service_response::Message", tags="1")]
    pub message: ::std::option::Option<user_service_response::Message>,
}
pub mod user_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Message {
        #[prost(message, tag="1")]
        GetUserResponse(super::GetUserResponse),
    }
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::User;

    #[derive(Debug, Clone)]
    pub struct UserController<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> UserController<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn get_user<R>(&mut self, request: grpc::Request<super::super::common::UserId>) -> grpc::unary::ResponseFuture<User, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::UserId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/user.UserController/GetUser");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::User;

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait UserController: Clone {
        type GetUserFuture: futures::Future<Item = grpc::Response<User>, Error = grpc::Status>;

        fn get_user(&mut self, request: grpc::Request<super::super::common::UserId>) -> Self::GetUserFuture;
    }

    #[derive(Debug, Clone)]
    pub struct UserControllerServer<T> {
        user_controller: T,
    }

    impl<T> UserControllerServer<T>
    where T: UserController,
    {
        pub fn new(user_controller: T) -> Self {
            Self { user_controller }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for UserControllerServer<T>
    where T: UserController,
    {
        type Response = http::Response<user_controller::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = user_controller::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::user_controller::Kind::*;

            match request.uri().path() {
                "/user.UserController/GetUser" => {
                    let service = user_controller::methods::GetUser(self.user_controller.clone());
                    let response = grpc::unary(service, request);
                    user_controller::ResponseFuture { kind: GetUser(response) }
                }
                _ => {
                    user_controller::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for UserControllerServer<T>
    where T: UserController,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for UserControllerServer<T>
    where T: UserController,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod user_controller {
        use ::tower_grpc::codegen::server::*;
        use super::UserController;

        pub struct ResponseFuture<T>
        where T: UserController,
        {
            pub(super) kind: Kind<
                // GetUser
                grpc::unary::ResponseFuture<methods::GetUser<T>, grpc::BoxBody, super::super::super::common::UserId>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: UserController,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    GetUser(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetUser(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: UserController,
        {
            pub(super) kind: Kind<
                // GetUser
                grpc::Encode<grpc::unary::Once<<methods::GetUser<T> as grpc::UnaryService<super::super::super::common::UserId>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: UserController,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    GetUser(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    GetUser(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    GetUser(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<GetUser, __Generated__Unimplemented> {
            GetUser(GetUser),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{UserController, User};

            pub struct GetUser<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::UserId>> for GetUser<T>
            where T: UserController,
            {
                type Response = grpc::Response<User>;
                type Error = grpc::Status;
                type Future = T::GetUserFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::UserId>) -> Self::Future {
                    self.0.get_user(request)
                }
            }
        }
    }
}
