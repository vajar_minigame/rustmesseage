#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonConsumeItemRequest {
    #[prost(message, optional, tag="1")]
    pub item_id: ::std::option::Option<super::common::ItemId>,
    #[prost(message, optional, tag="2")]
    pub mon_id: ::std::option::Option<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserConsumeItemRequest {
    #[prost(message, optional, tag="1")]
    pub item_id: ::std::option::Option<super::common::ItemId>,
    #[prost(message, optional, tag="2")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct InteractionServiceCall {
    #[prost(message, optional, tag="1")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof="interaction_service_call::Method", tags="2")]
    pub method: ::std::option::Option<interaction_service_call::Method>,
}
pub mod interaction_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Method {
        #[prost(message, tag="2")]
        ConsumeItem(super::MonConsumeItemRequest),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct FeedMonResponse {
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct InteractionServiceResponse {
    #[prost(message, optional, tag="1")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof="interaction_service_response::Data", tags="2")]
    pub data: ::std::option::Option<interaction_service_response::Data>,
}
pub mod interaction_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag="2")]
        Status(super::super::common::ResponseStatus),
    }
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{MonConsumeItemRequest, UserConsumeItemRequest};

    #[derive(Debug, Clone)]
    pub struct InteractionService<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> InteractionService<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn monster_consume_item<R>(&mut self, request: grpc::Request<MonConsumeItemRequest>) -> grpc::unary::ResponseFuture<super::super::common::ResponseStatus, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<MonConsumeItemRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/interaction.InteractionService/MonsterConsumeItem");
            self.inner.unary(request, path)
        }

        pub fn user_consume_item<R>(&mut self, request: grpc::Request<UserConsumeItemRequest>) -> grpc::unary::ResponseFuture<super::super::common::ResponseStatus, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<UserConsumeItemRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/interaction.InteractionService/UserConsumeItem");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{MonConsumeItemRequest, UserConsumeItemRequest};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait InteractionService: Clone {
        type MonsterConsumeItemFuture: futures::Future<Item = grpc::Response<super::super::common::ResponseStatus>, Error = grpc::Status>;
        type UserConsumeItemFuture: futures::Future<Item = grpc::Response<super::super::common::ResponseStatus>, Error = grpc::Status>;

        fn monster_consume_item(&mut self, request: grpc::Request<MonConsumeItemRequest>) -> Self::MonsterConsumeItemFuture;

        fn user_consume_item(&mut self, request: grpc::Request<UserConsumeItemRequest>) -> Self::UserConsumeItemFuture;
    }

    #[derive(Debug, Clone)]
    pub struct InteractionServiceServer<T> {
        interaction_service: T,
    }

    impl<T> InteractionServiceServer<T>
    where T: InteractionService,
    {
        pub fn new(interaction_service: T) -> Self {
            Self { interaction_service }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for InteractionServiceServer<T>
    where T: InteractionService,
    {
        type Response = http::Response<interaction_service::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = interaction_service::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::interaction_service::Kind::*;

            match request.uri().path() {
                "/interaction.InteractionService/MonsterConsumeItem" => {
                    let service = interaction_service::methods::MonsterConsumeItem(self.interaction_service.clone());
                    let response = grpc::unary(service, request);
                    interaction_service::ResponseFuture { kind: MonsterConsumeItem(response) }
                }
                "/interaction.InteractionService/UserConsumeItem" => {
                    let service = interaction_service::methods::UserConsumeItem(self.interaction_service.clone());
                    let response = grpc::unary(service, request);
                    interaction_service::ResponseFuture { kind: UserConsumeItem(response) }
                }
                _ => {
                    interaction_service::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for InteractionServiceServer<T>
    where T: InteractionService,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for InteractionServiceServer<T>
    where T: InteractionService,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod interaction_service {
        use ::tower_grpc::codegen::server::*;
        use super::InteractionService;
        use super::super::{MonConsumeItemRequest, UserConsumeItemRequest};

        pub struct ResponseFuture<T>
        where T: InteractionService,
        {
            pub(super) kind: Kind<
                // MonsterConsumeItem
                grpc::unary::ResponseFuture<methods::MonsterConsumeItem<T>, grpc::BoxBody, MonConsumeItemRequest>,
                // UserConsumeItem
                grpc::unary::ResponseFuture<methods::UserConsumeItem<T>, grpc::BoxBody, UserConsumeItemRequest>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: InteractionService,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    MonsterConsumeItem(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: MonsterConsumeItem(body) }
                        });
                        Ok(response.into())
                    }
                    UserConsumeItem(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: UserConsumeItem(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: InteractionService,
        {
            pub(super) kind: Kind<
                // MonsterConsumeItem
                grpc::Encode<grpc::unary::Once<<methods::MonsterConsumeItem<T> as grpc::UnaryService<MonConsumeItemRequest>>::Response>>,
                // UserConsumeItem
                grpc::Encode<grpc::unary::Once<<methods::UserConsumeItem<T> as grpc::UnaryService<UserConsumeItemRequest>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: InteractionService,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    MonsterConsumeItem(ref v) => v.is_end_stream(),
                    UserConsumeItem(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    MonsterConsumeItem(ref mut v) => v.poll_data(),
                    UserConsumeItem(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    MonsterConsumeItem(ref mut v) => v.poll_trailers(),
                    UserConsumeItem(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<MonsterConsumeItem, UserConsumeItem, __Generated__Unimplemented> {
            MonsterConsumeItem(MonsterConsumeItem),
            UserConsumeItem(UserConsumeItem),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{InteractionService, MonConsumeItemRequest, UserConsumeItemRequest};

            pub struct MonsterConsumeItem<T>(pub T);

            impl<T> tower::Service<grpc::Request<MonConsumeItemRequest>> for MonsterConsumeItem<T>
            where T: InteractionService,
            {
                type Response = grpc::Response<super::super::super::super::common::ResponseStatus>;
                type Error = grpc::Status;
                type Future = T::MonsterConsumeItemFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<MonConsumeItemRequest>) -> Self::Future {
                    self.0.monster_consume_item(request)
                }
            }

            pub struct UserConsumeItem<T>(pub T);

            impl<T> tower::Service<grpc::Request<UserConsumeItemRequest>> for UserConsumeItem<T>
            where T: InteractionService,
            {
                type Response = grpc::Response<super::super::super::super::common::ResponseStatus>;
                type Error = grpc::Status;
                type Future = T::UserConsumeItemFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<UserConsumeItemRequest>) -> Self::Future {
                    self.0.user_consume_item(request)
                }
            }
        }
    }
}
