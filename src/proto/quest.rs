#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Quest {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::QuestId>,
    #[prost(int32, tag="2")]
    pub difficulty: i32,
    #[prost(int32, tag="3")]
    pub recommended_strength: i32,
    #[prost(int32, tag="4")]
    pub player_id: i32,
    /// duration in seconds
    #[prost(int32, tag="5")]
    pub duration: i32,
    #[prost(message, optional, tag="6")]
    pub active_quest: ::std::option::Option<ActiveQuest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ActiveQuest {
    #[prost(message, optional, tag="1")]
    pub start_time: ::std::option::Option<::prost_types::Timestamp>,
    #[prost(message, repeated, tag="2")]
    pub monster_ids: ::std::vec::Vec<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct StartQuestRequest {
    #[prost(message, optional, tag="1")]
    pub quest_id: ::std::option::Option<super::common::QuestId>,
    #[prost(message, repeated, tag="2")]
    pub monster_ids: ::std::vec::Vec<super::common::MonId>,
}
//////////////////////////////////////////////////////////////////

#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetQuestsByUser {
    #[prost(message, optional, tag="1")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetQuest {
    #[prost(message, optional, tag="1")]
    pub quest_id: ::std::option::Option<super::common::QuestId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct StartQuestResponse {
    #[prost(message, optional, tag="1")]
    pub status: ::std::option::Option<super::common::ResponseStatus>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetQuestResponse {
    #[prost(message, optional, tag="1")]
    pub quest: ::std::option::Option<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetQuestByUserResponse {
    #[prost(message, repeated, tag="1")]
    pub quests: ::std::vec::Vec<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestList {
    #[prost(message, repeated, tag="1")]
    pub quests: ::std::vec::Vec<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestCompletedEvent {
    #[prost(message, optional, tag="1")]
    pub quest_id: ::std::option::Option<super::common::QuestId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestUpdatedEvent {
    #[prost(message, optional, tag="1")]
    pub quest: ::std::option::Option<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestDeletedEvent {
    #[prost(message, optional, tag="1")]
    pub quest_id: ::std::option::Option<super::common::QuestId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestStartedEvent {
    #[prost(message, optional, tag="1")]
    pub quest: ::std::option::Option<Quest>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestEvent {
    #[prost(oneof="quest_event::Event", tags="1, 2, 3, 4")]
    pub event: ::std::option::Option<quest_event::Event>,
}
pub mod quest_event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag="1")]
        CompletedEvent(super::QuestCompletedEvent),
        #[prost(message, tag="2")]
        UpdatedEvent(super::QuestUpdatedEvent),
        #[prost(message, tag="3")]
        DeletedEvent(super::QuestDeletedEvent),
        #[prost(message, tag="4")]
        StartedEvent(super::QuestStartedEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestServiceCall {
    #[prost(message, optional, tag="4")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof="quest_service_call::Method", tags="1, 2, 3")]
    pub method: ::std::option::Option<quest_service_call::Method>,
}
pub mod quest_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Method {
        #[prost(message, tag="1")]
        StartQuest(super::StartQuestRequest),
        #[prost(message, tag="2")]
        GetQuest(super::GetQuest),
        #[prost(message, tag="3")]
        QuestQuestByUser(super::GetQuestsByUser),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestServiceResponse {
    #[prost(message, optional, tag="4")]
    pub meta_data: ::std::option::Option<super::common::MetaData>,
    #[prost(oneof="quest_service_response::Data", tags="1, 2, 3")]
    pub data: ::std::option::Option<quest_service_response::Data>,
}
pub mod quest_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag="1")]
        StartQuestResponse(super::StartQuestResponse),
        #[prost(message, tag="2")]
        GetQuestResponse(super::GetQuestResponse),
        #[prost(message, tag="3")]
        GetQuestByUserResponse(super::GetQuestByUserResponse),
    }
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{Quest, QuestList, StartQuestRequest};

    #[derive(Debug, Clone)]
    pub struct QuestServices<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> QuestServices<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn add_quest<R>(&mut self, request: grpc::Request<Quest>) -> grpc::unary::ResponseFuture<super::super::common::QuestId, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<Quest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/quest.QuestServices/AddQuest");
            self.inner.unary(request, path)
        }

        pub fn get_quest_by_id<R>(&mut self, request: grpc::Request<super::super::common::QuestId>) -> grpc::unary::ResponseFuture<Quest, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::QuestId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/quest.QuestServices/GetQuestByID");
            self.inner.unary(request, path)
        }

        pub fn get_quest_by_user_id<R>(&mut self, request: grpc::Request<super::super::common::UserId>) -> grpc::unary::ResponseFuture<QuestList, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::UserId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/quest.QuestServices/GetQuestByUserID");
            self.inner.unary(request, path)
        }

        pub fn start_quest<R>(&mut self, request: grpc::Request<StartQuestRequest>) -> grpc::unary::ResponseFuture<super::super::common::ResponseStatus, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<StartQuestRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/quest.QuestServices/StartQuest");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{Quest, QuestList, StartQuestRequest};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait QuestServices: Clone {
        type AddQuestFuture: futures::Future<Item = grpc::Response<super::super::common::QuestId>, Error = grpc::Status>;
        type GetQuestByIdFuture: futures::Future<Item = grpc::Response<Quest>, Error = grpc::Status>;
        type GetQuestByUserIdFuture: futures::Future<Item = grpc::Response<QuestList>, Error = grpc::Status>;
        type StartQuestFuture: futures::Future<Item = grpc::Response<super::super::common::ResponseStatus>, Error = grpc::Status>;

        fn add_quest(&mut self, request: grpc::Request<Quest>) -> Self::AddQuestFuture;

        fn get_quest_by_id(&mut self, request: grpc::Request<super::super::common::QuestId>) -> Self::GetQuestByIdFuture;

        fn get_quest_by_user_id(&mut self, request: grpc::Request<super::super::common::UserId>) -> Self::GetQuestByUserIdFuture;

        fn start_quest(&mut self, request: grpc::Request<StartQuestRequest>) -> Self::StartQuestFuture;
    }

    #[derive(Debug, Clone)]
    pub struct QuestServicesServer<T> {
        quest_services: T,
    }

    impl<T> QuestServicesServer<T>
    where T: QuestServices,
    {
        pub fn new(quest_services: T) -> Self {
            Self { quest_services }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for QuestServicesServer<T>
    where T: QuestServices,
    {
        type Response = http::Response<quest_services::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = quest_services::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::quest_services::Kind::*;

            match request.uri().path() {
                "/quest.QuestServices/AddQuest" => {
                    let service = quest_services::methods::AddQuest(self.quest_services.clone());
                    let response = grpc::unary(service, request);
                    quest_services::ResponseFuture { kind: AddQuest(response) }
                }
                "/quest.QuestServices/GetQuestByID" => {
                    let service = quest_services::methods::GetQuestById(self.quest_services.clone());
                    let response = grpc::unary(service, request);
                    quest_services::ResponseFuture { kind: GetQuestById(response) }
                }
                "/quest.QuestServices/GetQuestByUserID" => {
                    let service = quest_services::methods::GetQuestByUserId(self.quest_services.clone());
                    let response = grpc::unary(service, request);
                    quest_services::ResponseFuture { kind: GetQuestByUserId(response) }
                }
                "/quest.QuestServices/StartQuest" => {
                    let service = quest_services::methods::StartQuest(self.quest_services.clone());
                    let response = grpc::unary(service, request);
                    quest_services::ResponseFuture { kind: StartQuest(response) }
                }
                _ => {
                    quest_services::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for QuestServicesServer<T>
    where T: QuestServices,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for QuestServicesServer<T>
    where T: QuestServices,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod quest_services {
        use ::tower_grpc::codegen::server::*;
        use super::QuestServices;
        use super::super::{Quest, StartQuestRequest};

        pub struct ResponseFuture<T>
        where T: QuestServices,
        {
            pub(super) kind: Kind<
                // AddQuest
                grpc::unary::ResponseFuture<methods::AddQuest<T>, grpc::BoxBody, Quest>,
                // GetQuestByID
                grpc::unary::ResponseFuture<methods::GetQuestById<T>, grpc::BoxBody, super::super::super::common::QuestId>,
                // GetQuestByUserID
                grpc::unary::ResponseFuture<methods::GetQuestByUserId<T>, grpc::BoxBody, super::super::super::common::UserId>,
                // StartQuest
                grpc::unary::ResponseFuture<methods::StartQuest<T>, grpc::BoxBody, StartQuestRequest>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: QuestServices,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddQuest(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: AddQuest(body) }
                        });
                        Ok(response.into())
                    }
                    GetQuestById(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetQuestById(body) }
                        });
                        Ok(response.into())
                    }
                    GetQuestByUserId(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetQuestByUserId(body) }
                        });
                        Ok(response.into())
                    }
                    StartQuest(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: StartQuest(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: QuestServices,
        {
            pub(super) kind: Kind<
                // AddQuest
                grpc::Encode<grpc::unary::Once<<methods::AddQuest<T> as grpc::UnaryService<Quest>>::Response>>,
                // GetQuestByID
                grpc::Encode<grpc::unary::Once<<methods::GetQuestById<T> as grpc::UnaryService<super::super::super::common::QuestId>>::Response>>,
                // GetQuestByUserID
                grpc::Encode<grpc::unary::Once<<methods::GetQuestByUserId<T> as grpc::UnaryService<super::super::super::common::UserId>>::Response>>,
                // StartQuest
                grpc::Encode<grpc::unary::Once<<methods::StartQuest<T> as grpc::UnaryService<StartQuestRequest>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: QuestServices,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    AddQuest(ref v) => v.is_end_stream(),
                    GetQuestById(ref v) => v.is_end_stream(),
                    GetQuestByUserId(ref v) => v.is_end_stream(),
                    StartQuest(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddQuest(ref mut v) => v.poll_data(),
                    GetQuestById(ref mut v) => v.poll_data(),
                    GetQuestByUserId(ref mut v) => v.poll_data(),
                    StartQuest(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddQuest(ref mut v) => v.poll_trailers(),
                    GetQuestById(ref mut v) => v.poll_trailers(),
                    GetQuestByUserId(ref mut v) => v.poll_trailers(),
                    StartQuest(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<AddQuest, GetQuestById, GetQuestByUserId, StartQuest, __Generated__Unimplemented> {
            AddQuest(AddQuest),
            GetQuestById(GetQuestById),
            GetQuestByUserId(GetQuestByUserId),
            StartQuest(StartQuest),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{QuestServices, Quest, QuestList, StartQuestRequest};

            pub struct AddQuest<T>(pub T);

            impl<T> tower::Service<grpc::Request<Quest>> for AddQuest<T>
            where T: QuestServices,
            {
                type Response = grpc::Response<super::super::super::super::common::QuestId>;
                type Error = grpc::Status;
                type Future = T::AddQuestFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<Quest>) -> Self::Future {
                    self.0.add_quest(request)
                }
            }

            pub struct GetQuestById<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::QuestId>> for GetQuestById<T>
            where T: QuestServices,
            {
                type Response = grpc::Response<Quest>;
                type Error = grpc::Status;
                type Future = T::GetQuestByIdFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::QuestId>) -> Self::Future {
                    self.0.get_quest_by_id(request)
                }
            }

            pub struct GetQuestByUserId<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::UserId>> for GetQuestByUserId<T>
            where T: QuestServices,
            {
                type Response = grpc::Response<QuestList>;
                type Error = grpc::Status;
                type Future = T::GetQuestByUserIdFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::UserId>) -> Self::Future {
                    self.0.get_quest_by_user_id(request)
                }
            }

            pub struct StartQuest<T>(pub T);

            impl<T> tower::Service<grpc::Request<StartQuestRequest>> for StartQuest<T>
            where T: QuestServices,
            {
                type Response = grpc::Response<super::super::super::super::common::ResponseStatus>;
                type Error = grpc::Status;
                type Future = T::StartQuestFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<StartQuestRequest>) -> Self::Future {
                    self.0.start_quest(request)
                }
            }
        }
    }
}
