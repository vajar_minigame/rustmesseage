#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Team {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::UserId>,
    #[prost(message, repeated, tag="2")]
    pub mons: ::std::vec::Vec<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Battle {
    #[prost(int32, tag="1")]
    pub id: i32,
    #[prost(message, optional, tag="2")]
    pub team_a: ::std::option::Option<Team>,
    #[prost(message, optional, tag="3")]
    pub team_b: ::std::option::Option<Team>,
    #[prost(message, optional, tag="4")]
    pub active_battle: ::std::option::Option<ActiveBattle>,
    #[prost(message, optional, tag="5")]
    pub last_update: ::std::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ActiveBattle {
    #[prost(message, optional, tag="1")]
    pub start_time: ::std::option::Option<::prost_types::Timestamp>,
    #[prost(int32, tag="3")]
    pub turn_count: i32,
    #[prost(message, repeated, tag="4")]
    pub turn_queue: ::std::vec::Vec<super::common::MonId>,
    /// implies how much time there is for the next action
    #[prost(message, optional, tag="5")]
    pub last_action: ::std::option::Option<::prost_types::Timestamp>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Report {
    #[prost(string, repeated, tag="1")]
    pub report: ::std::vec::Vec<std::string::String>,
}
/// whose turn is next?
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct TurnQueueEvent {
    #[prost(message, repeated, tag="1")]
    pub mon: ::std::vec::Vec<super::common::MonId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleStartedEvent {
    #[prost(message, optional, tag="1")]
    pub b: ::std::option::Option<Battle>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleEndedEvent {
    #[prost(message, optional, tag="1")]
    pub ev: ::std::option::Option<Report>,
    #[prost(message, optional, tag="2")]
    pub winning_team: ::std::option::Option<Team>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleEvent {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::BattleId>,
    #[prost(oneof="battle_event::Event", tags="2, 3, 4, 5")]
    pub event: ::std::option::Option<battle_event::Event>,
}
pub mod battle_event {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Event {
        #[prost(message, tag="2")]
        TurnUpdate(super::TurnQueueEvent),
        #[prost(message, tag="3")]
        Action(super::ActionEvent),
        #[prost(message, tag="4")]
        BattleStarted(super::BattleStartedEvent),
        #[prost(message, tag="5")]
        BattleEnded(super::BattleEndedEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ActionEvent {
    #[prost(message, optional, tag="1")]
    pub source: ::std::option::Option<super::common::MonId>,
    #[prost(message, optional, tag="2")]
    pub target: ::std::option::Option<super::common::MonId>,
    #[prost(message, optional, tag="3")]
    pub effect: ::std::option::Option<super::monster::BattleValues>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleResponse {
    #[prost(oneof="battle_response::Message", tags="1, 3")]
    pub message: ::std::option::Option<battle_response::Message>,
}
pub mod battle_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Message {
        #[prost(message, tag="1")]
        Ev(super::Report),
        #[prost(message, tag="3")]
        TurnU(super::TurnQueueEvent),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Command {
    #[prost(message, optional, tag="1")]
    pub battle_id: ::std::option::Option<super::common::BattleId>,
    #[prost(oneof="command::Command", tags="2, 3")]
    pub command: ::std::option::Option<command::Command>,
}
pub mod command {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Command {
        #[prost(message, tag="2")]
        Attack(super::Attack),
        #[prost(message, tag="3")]
        Random(super::RandomCommand),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleRequest {
    #[prost(message, optional, tag="1")]
    pub id: ::std::option::Option<super::common::BattleId>,
    #[prost(message, repeated, tag="2")]
    pub mon_ids: ::std::vec::Vec<super::common::MonId>,
    #[prost(message, optional, tag="3")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleList {
    #[prost(message, repeated, tag="1")]
    pub battles: ::std::vec::Vec<Battle>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ResponseStatus {
    #[prost(bool, tag="1")]
    pub success: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RandomCommand {
    /// sanity check
    #[prost(message, optional, tag="1")]
    pub source: ::std::option::Option<super::common::MonId>,
}
/// later
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Attack {
    #[prost(message, optional, tag="1")]
    pub source: ::std::option::Option<super::common::MonId>,
    /// skill
    #[prost(message, optional, tag="2")]
    pub target: ::std::option::Option<super::common::MonId>,
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{Battle, BattleList, BattleRequest, ResponseStatus, Command};

    #[derive(Debug, Clone)]
    pub struct BattleService<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> BattleService<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn add_battle<R>(&mut self, request: grpc::Request<Battle>) -> grpc::unary::ResponseFuture<super::super::common::BattleId, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<Battle>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/battle.BattleService/AddBattle");
            self.inner.unary(request, path)
        }

        pub fn get_battle_by_id<R>(&mut self, request: grpc::Request<super::super::common::BattleId>) -> grpc::unary::ResponseFuture<Battle, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::BattleId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/battle.BattleService/getBattleByID");
            self.inner.unary(request, path)
        }

        pub fn get_battle_by_user_id<R>(&mut self, request: grpc::Request<super::super::common::UserId>) -> grpc::unary::ResponseFuture<BattleList, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<super::super::common::UserId>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/battle.BattleService/GetBattleByUserID");
            self.inner.unary(request, path)
        }

        pub fn start_battle<R>(&mut self, request: grpc::Request<BattleRequest>) -> grpc::unary::ResponseFuture<ResponseStatus, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<BattleRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/battle.BattleService/StartBattle");
            self.inner.unary(request, path)
        }

        pub fn command_request<R>(&mut self, request: grpc::Request<Command>) -> grpc::unary::ResponseFuture<ResponseStatus, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<Command>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/battle.BattleService/CommandRequest");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{Battle, BattleList, BattleRequest, ResponseStatus, Command};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait BattleService: Clone {
        type AddBattleFuture: futures::Future<Item = grpc::Response<super::super::common::BattleId>, Error = grpc::Status>;
        type GetBattleByIdFuture: futures::Future<Item = grpc::Response<Battle>, Error = grpc::Status>;
        type GetBattleByUserIdFuture: futures::Future<Item = grpc::Response<BattleList>, Error = grpc::Status>;
        type StartBattleFuture: futures::Future<Item = grpc::Response<ResponseStatus>, Error = grpc::Status>;
        type CommandRequestFuture: futures::Future<Item = grpc::Response<ResponseStatus>, Error = grpc::Status>;

        fn add_battle(&mut self, request: grpc::Request<Battle>) -> Self::AddBattleFuture;

        fn get_battle_by_id(&mut self, request: grpc::Request<super::super::common::BattleId>) -> Self::GetBattleByIdFuture;

        fn get_battle_by_user_id(&mut self, request: grpc::Request<super::super::common::UserId>) -> Self::GetBattleByUserIdFuture;

        fn start_battle(&mut self, request: grpc::Request<BattleRequest>) -> Self::StartBattleFuture;

        fn command_request(&mut self, request: grpc::Request<Command>) -> Self::CommandRequestFuture;
    }

    #[derive(Debug, Clone)]
    pub struct BattleServiceServer<T> {
        battle_service: T,
    }

    impl<T> BattleServiceServer<T>
    where T: BattleService,
    {
        pub fn new(battle_service: T) -> Self {
            Self { battle_service }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for BattleServiceServer<T>
    where T: BattleService,
    {
        type Response = http::Response<battle_service::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = battle_service::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::battle_service::Kind::*;

            match request.uri().path() {
                "/battle.BattleService/AddBattle" => {
                    let service = battle_service::methods::AddBattle(self.battle_service.clone());
                    let response = grpc::unary(service, request);
                    battle_service::ResponseFuture { kind: AddBattle(response) }
                }
                "/battle.BattleService/getBattleByID" => {
                    let service = battle_service::methods::GetBattleById(self.battle_service.clone());
                    let response = grpc::unary(service, request);
                    battle_service::ResponseFuture { kind: GetBattleById(response) }
                }
                "/battle.BattleService/GetBattleByUserID" => {
                    let service = battle_service::methods::GetBattleByUserId(self.battle_service.clone());
                    let response = grpc::unary(service, request);
                    battle_service::ResponseFuture { kind: GetBattleByUserId(response) }
                }
                "/battle.BattleService/StartBattle" => {
                    let service = battle_service::methods::StartBattle(self.battle_service.clone());
                    let response = grpc::unary(service, request);
                    battle_service::ResponseFuture { kind: StartBattle(response) }
                }
                "/battle.BattleService/CommandRequest" => {
                    let service = battle_service::methods::CommandRequest(self.battle_service.clone());
                    let response = grpc::unary(service, request);
                    battle_service::ResponseFuture { kind: CommandRequest(response) }
                }
                _ => {
                    battle_service::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for BattleServiceServer<T>
    where T: BattleService,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for BattleServiceServer<T>
    where T: BattleService,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod battle_service {
        use ::tower_grpc::codegen::server::*;
        use super::BattleService;
        use super::super::{Battle, BattleRequest, Command};

        pub struct ResponseFuture<T>
        where T: BattleService,
        {
            pub(super) kind: Kind<
                // AddBattle
                grpc::unary::ResponseFuture<methods::AddBattle<T>, grpc::BoxBody, Battle>,
                // getBattleByID
                grpc::unary::ResponseFuture<methods::GetBattleById<T>, grpc::BoxBody, super::super::super::common::BattleId>,
                // GetBattleByUserID
                grpc::unary::ResponseFuture<methods::GetBattleByUserId<T>, grpc::BoxBody, super::super::super::common::UserId>,
                // StartBattle
                grpc::unary::ResponseFuture<methods::StartBattle<T>, grpc::BoxBody, BattleRequest>,
                // CommandRequest
                grpc::unary::ResponseFuture<methods::CommandRequest<T>, grpc::BoxBody, Command>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: BattleService,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddBattle(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: AddBattle(body) }
                        });
                        Ok(response.into())
                    }
                    GetBattleById(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetBattleById(body) }
                        });
                        Ok(response.into())
                    }
                    GetBattleByUserId(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: GetBattleByUserId(body) }
                        });
                        Ok(response.into())
                    }
                    StartBattle(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: StartBattle(body) }
                        });
                        Ok(response.into())
                    }
                    CommandRequest(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: CommandRequest(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: BattleService,
        {
            pub(super) kind: Kind<
                // AddBattle
                grpc::Encode<grpc::unary::Once<<methods::AddBattle<T> as grpc::UnaryService<Battle>>::Response>>,
                // getBattleByID
                grpc::Encode<grpc::unary::Once<<methods::GetBattleById<T> as grpc::UnaryService<super::super::super::common::BattleId>>::Response>>,
                // GetBattleByUserID
                grpc::Encode<grpc::unary::Once<<methods::GetBattleByUserId<T> as grpc::UnaryService<super::super::super::common::UserId>>::Response>>,
                // StartBattle
                grpc::Encode<grpc::unary::Once<<methods::StartBattle<T> as grpc::UnaryService<BattleRequest>>::Response>>,
                // CommandRequest
                grpc::Encode<grpc::unary::Once<<methods::CommandRequest<T> as grpc::UnaryService<Command>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: BattleService,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    AddBattle(ref v) => v.is_end_stream(),
                    GetBattleById(ref v) => v.is_end_stream(),
                    GetBattleByUserId(ref v) => v.is_end_stream(),
                    StartBattle(ref v) => v.is_end_stream(),
                    CommandRequest(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddBattle(ref mut v) => v.poll_data(),
                    GetBattleById(ref mut v) => v.poll_data(),
                    GetBattleByUserId(ref mut v) => v.poll_data(),
                    StartBattle(ref mut v) => v.poll_data(),
                    CommandRequest(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    AddBattle(ref mut v) => v.poll_trailers(),
                    GetBattleById(ref mut v) => v.poll_trailers(),
                    GetBattleByUserId(ref mut v) => v.poll_trailers(),
                    StartBattle(ref mut v) => v.poll_trailers(),
                    CommandRequest(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<AddBattle, GetBattleById, GetBattleByUserId, StartBattle, CommandRequest, __Generated__Unimplemented> {
            AddBattle(AddBattle),
            GetBattleById(GetBattleById),
            GetBattleByUserId(GetBattleByUserId),
            StartBattle(StartBattle),
            CommandRequest(CommandRequest),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{BattleService, Battle, BattleList, BattleRequest, ResponseStatus, Command};

            pub struct AddBattle<T>(pub T);

            impl<T> tower::Service<grpc::Request<Battle>> for AddBattle<T>
            where T: BattleService,
            {
                type Response = grpc::Response<super::super::super::super::common::BattleId>;
                type Error = grpc::Status;
                type Future = T::AddBattleFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<Battle>) -> Self::Future {
                    self.0.add_battle(request)
                }
            }

            pub struct GetBattleById<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::BattleId>> for GetBattleById<T>
            where T: BattleService,
            {
                type Response = grpc::Response<Battle>;
                type Error = grpc::Status;
                type Future = T::GetBattleByIdFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::BattleId>) -> Self::Future {
                    self.0.get_battle_by_id(request)
                }
            }

            pub struct GetBattleByUserId<T>(pub T);

            impl<T> tower::Service<grpc::Request<super::super::super::super::common::UserId>> for GetBattleByUserId<T>
            where T: BattleService,
            {
                type Response = grpc::Response<BattleList>;
                type Error = grpc::Status;
                type Future = T::GetBattleByUserIdFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<super::super::super::super::common::UserId>) -> Self::Future {
                    self.0.get_battle_by_user_id(request)
                }
            }

            pub struct StartBattle<T>(pub T);

            impl<T> tower::Service<grpc::Request<BattleRequest>> for StartBattle<T>
            where T: BattleService,
            {
                type Response = grpc::Response<ResponseStatus>;
                type Error = grpc::Status;
                type Future = T::StartBattleFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<BattleRequest>) -> Self::Future {
                    self.0.start_battle(request)
                }
            }

            pub struct CommandRequest<T>(pub T);

            impl<T> tower::Service<grpc::Request<Command>> for CommandRequest<T>
            where T: BattleService,
            {
                type Response = grpc::Response<ResponseStatus>;
                type Error = grpc::Status;
                type Future = T::CommandRequestFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<Command>) -> Self::Future {
                    self.0.command_request(request)
                }
            }
        }
    }
}
