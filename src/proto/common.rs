#[derive(Clone, PartialEq, ::prost::Message)]
pub struct QuestId {
    #[prost(int32, tag="1")]
    pub id: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MonId {
    #[prost(int32, tag="1")]
    pub id: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserId {
    #[prost(int32, tag="1")]
    pub id: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ItemId {
    #[prost(int32, tag="1")]
    pub id: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MessageId {
    #[prost(int32, tag="1")]
    pub id: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct BattleId {
    #[prost(int32, tag="1")]
    pub id: i32,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ResponseStatus {
    #[prost(bool, tag="1")]
    pub success: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MetaData {
    #[prost(int32, tag="1")]
    pub message_id: i32,
    #[prost(int32, tag="2")]
    pub corr_id: i32,
    #[prost(string, tag="3")]
    pub jwt: std::string::String,
}
