#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LoginCall {
    #[prost(string, tag="1")]
    pub username: std::string::String,
    #[prost(string, tag="2")]
    pub password: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LogoutCall {
    #[prost(message, optional, tag="1")]
    pub user_id: ::std::option::Option<super::common::UserId>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LoginResponse {
    #[prost(string, tag="1")]
    pub token: std::string::String,
    #[prost(message, optional, tag="2")]
    pub user: ::std::option::Option<super::user::User>,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LogoutResponse {
    #[prost(bool, tag="1")]
    pub successful: bool,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AuthServiceCall {
    #[prost(oneof="auth_service_call::Message", tags="1, 2")]
    pub message: ::std::option::Option<auth_service_call::Message>,
}
pub mod auth_service_call {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Message {
        #[prost(message, tag="1")]
        LoginCall(super::LoginCall),
        #[prost(message, tag="2")]
        LogoutCall(super::LogoutCall),
    }
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AuthServiceResponse {
    #[prost(oneof="auth_service_response::Message", tags="1, 2")]
    pub message: ::std::option::Option<auth_service_response::Message>,
}
pub mod auth_service_response {
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Message {
        #[prost(message, tag="1")]
        LoginResponse(super::LoginResponse),
        #[prost(message, tag="2")]
        LogoutRespone(super::LogoutResponse),
    }
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{LoginCall, LoginResponse, LogoutCall, LogoutResponse};

    #[derive(Debug, Clone)]
    pub struct Auth<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> Auth<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn login<R>(&mut self, request: grpc::Request<LoginCall>) -> grpc::unary::ResponseFuture<LoginResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<LoginCall>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/auth.Auth/Login");
            self.inner.unary(request, path)
        }

        pub fn logout<R>(&mut self, request: grpc::Request<LogoutCall>) -> grpc::unary::ResponseFuture<LogoutResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<LogoutCall>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/auth.Auth/Logout");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{LoginCall, LoginResponse, LogoutCall, LogoutResponse};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait Auth: Clone {
        type LoginFuture: futures::Future<Item = grpc::Response<LoginResponse>, Error = grpc::Status>;
        type LogoutFuture: futures::Future<Item = grpc::Response<LogoutResponse>, Error = grpc::Status>;

        fn login(&mut self, request: grpc::Request<LoginCall>) -> Self::LoginFuture;

        fn logout(&mut self, request: grpc::Request<LogoutCall>) -> Self::LogoutFuture;
    }

    #[derive(Debug, Clone)]
    pub struct AuthServer<T> {
        auth: T,
    }

    impl<T> AuthServer<T>
    where T: Auth,
    {
        pub fn new(auth: T) -> Self {
            Self { auth }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for AuthServer<T>
    where T: Auth,
    {
        type Response = http::Response<auth::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = auth::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::auth::Kind::*;

            match request.uri().path() {
                "/auth.Auth/Login" => {
                    let service = auth::methods::Login(self.auth.clone());
                    let response = grpc::unary(service, request);
                    auth::ResponseFuture { kind: Login(response) }
                }
                "/auth.Auth/Logout" => {
                    let service = auth::methods::Logout(self.auth.clone());
                    let response = grpc::unary(service, request);
                    auth::ResponseFuture { kind: Logout(response) }
                }
                _ => {
                    auth::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for AuthServer<T>
    where T: Auth,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for AuthServer<T>
    where T: Auth,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod auth {
        use ::tower_grpc::codegen::server::*;
        use super::Auth;
        use super::super::{LoginCall, LogoutCall};

        pub struct ResponseFuture<T>
        where T: Auth,
        {
            pub(super) kind: Kind<
                // Login
                grpc::unary::ResponseFuture<methods::Login<T>, grpc::BoxBody, LoginCall>,
                // Logout
                grpc::unary::ResponseFuture<methods::Logout<T>, grpc::BoxBody, LogoutCall>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: Auth,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    Login(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: Login(body) }
                        });
                        Ok(response.into())
                    }
                    Logout(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: Logout(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: Auth,
        {
            pub(super) kind: Kind<
                // Login
                grpc::Encode<grpc::unary::Once<<methods::Login<T> as grpc::UnaryService<LoginCall>>::Response>>,
                // Logout
                grpc::Encode<grpc::unary::Once<<methods::Logout<T> as grpc::UnaryService<LogoutCall>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: Auth,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    Login(ref v) => v.is_end_stream(),
                    Logout(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    Login(ref mut v) => v.poll_data(),
                    Logout(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    Login(ref mut v) => v.poll_trailers(),
                    Logout(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<Login, Logout, __Generated__Unimplemented> {
            Login(Login),
            Logout(Logout),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{Auth, LoginCall, LoginResponse, LogoutCall, LogoutResponse};

            pub struct Login<T>(pub T);

            impl<T> tower::Service<grpc::Request<LoginCall>> for Login<T>
            where T: Auth,
            {
                type Response = grpc::Response<LoginResponse>;
                type Error = grpc::Status;
                type Future = T::LoginFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<LoginCall>) -> Self::Future {
                    self.0.login(request)
                }
            }

            pub struct Logout<T>(pub T);

            impl<T> tower::Service<grpc::Request<LogoutCall>> for Logout<T>
            where T: Auth,
            {
                type Response = grpc::Response<LogoutResponse>;
                type Error = grpc::Status;
                type Future = T::LogoutFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<LogoutCall>) -> Self::Future {
                    self.0.logout(request)
                }
            }
        }
    }
}
