extern crate chrono;
use crate::storage::DBHandler;
use futures::future::lazy;
use futures::sync::mpsc;
use futures::{future, Future, Sink, Stream};
use log::warn;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use chrono::{DateTime, NaiveDateTime, Utc};

use tower_grpc as grpc;

use crate::proto::{chat, common};

use chat::server::ChatService;

#[derive(Debug)]
struct ClientConn {
    sender: mpsc::Sender<chat::Event>,
}

#[derive(Debug)]
struct State {
    client_conns: Mutex<HashMap<i32, ClientConn>>,
}

#[derive(Clone)]
pub struct ChatServe {
    state: Arc<State>,
    db_handler: super::super::storage::PostgresConn,
}

use crate::diesel::ExpressionMethods;
use crate::diesel::{QueryDsl, RunQueryDsl};
use crate::storage::db_models::Subscription;
use crate::storage::schema::sub_user_room::dsl::sub_user_room;
use std::ops::Deref;
impl ChatService for ChatServe {
    type SendMessageFuture = future::FutureResult<grpc::Response<chat::SendResponse>, grpc::Status>;
    type GetEventStreamStream = Box<dyn Stream<Item = chat::Event, Error = grpc::Status> + Send>;
    type GetEventStreamFuture =
        future::FutureResult<grpc::Response<Self::GetEventStreamStream>, grpc::Status>;
    type JoinRoomFuture = future::FutureResult<grpc::Response<chat::JoinResponse>, grpc::Status>;
    type LeaveRoomFuture = future::FutureResult<grpc::Response<chat::LeaveResponse>, grpc::Status>;
    type GetRoomsByUserFuture = future::FutureResult<grpc::Response<chat::Rooms>, grpc::Status>;
    type GetAllRoomsFuture = future::FutureResult<grpc::Response<chat::Rooms>, grpc::Status>;
    type GetLastMessagesByRoomFuture =
        future::FutureResult<grpc::Response<chat::Messages>, grpc::Status>;

    // write message to db and push it out to all other users in the room
    fn send_message(&mut self, request: grpc::Request<chat::Message>) -> Self::SendMessageFuture {
        let req = request.get_ref();
        let room_id: i32;

        match req.room_id.as_ref() {
            Some(v) => room_id = v.id,
            None => {
                warn!("The request is invalid");
                return future::err(grpc::Status::new(
                    grpc::Code::InvalidArgument,
                    "room is wrong",
                ));
            }
        }
        let user_id = req.sender.as_ref().unwrap().id;
        let timestamp = req.sent_time.as_ref().unwrap();
        let ndt = NaiveDateTime::from_timestamp(timestamp.seconds, timestamp.nanos as u32);

        let time = DateTime::<Utc>::from_utc(ndt, Utc);

        let body: &str = req.body.as_ref();

        let msg = self.db_handler.create_message(
            user_id,
            std::time::SystemTime::from(time),
            room_id,
            &body,
        );

        match msg {
            Some(_) => (),
            None => {
                return future::err(grpc::Status::new(
                    grpc::Code::InvalidArgument,
                    "message couldn't be saved",
                ));
            }
        }
        // get a list of all users that are subbed to that room
        let msg = sub_user_room
            .filter(crate::storage::schema::sub_user_room::columns::room_id.eq(room_id))
            .load::<Subscription>(self.db_handler.conn.get().unwrap().deref());

        let state = self.state.clone();
        let req_clone = req.clone();
        tokio::spawn(lazy(move || {
            let conns = state.client_conns.lock().unwrap();
            match msg {
                Ok(v) => {
                    for sub in &v {
                        let client = conns.get(&sub.userid);
                        match client {
                            Some(c) => {
                                let event = chat::Event {
                                    event: Some(chat::event::Event::Msg(req_clone.clone())),
                                };
                                c.sender.clone().send(event).wait();
                            }
                            None => warn!(
                                "error sending message to all subscribed users {}",
                                sub.userid
                            ),
                        };
                    }
                }
                Err(e) => {
                    warn!("error sending message to all subscribed users: {}", e);
                }
            }
            return Ok(());
        }));

        let response = chat::SendResponse {};
        //println!("{:?}", request);

        let res = grpc::Response::new(response);
        future::ok(res)
    }

    // registers user for the event stream
    fn get_event_stream(
        &mut self,
        request: grpc::Request<common::UserId>,
    ) -> Self::GetEventStreamFuture {
        let (tx, rx) = futures::sync::mpsc::channel::<chat::Event>(4);

        let req = request.get_ref();
        let user_id = req.id;

        let mut conns = self.state.client_conns.lock().unwrap();

        //let tx2 = tx.clone();
        //tx2.send(test_msg.clone()).wait().unwrap();
        conns.insert(user_id, ClientConn { sender: tx.clone() });

        let rx = rx.map_err(|_| unimplemented!());
        future::ok(grpc::Response::new(Box::new(rx)))
    }

    // check if room exists
    // can only have on room opened at a time
    fn join_room(&mut self, request: grpc::Request<chat::JoinRequest>) -> Self::JoinRoomFuture {
        let req = request.get_ref();

        let room_id = req.room_id.as_ref().unwrap().id;
        let user_id = req.user_id.as_ref().unwrap().id;

        let sub = self.db_handler.subscribe_user_room(user_id, room_id);

        match sub {
            Some(_) => println!("added user:{} to room: {}", user_id, room_id),
            None => {
                warn!("The request is invalid");
                return future::err(grpc::Status::new(
                    grpc::Code::AlreadyExists,
                    "database error",
                ));
            }
        }

        let response = chat::JoinResponse {};
        let res = grpc::Response::new(response);

        future::ok(res)
    }

    fn leave_room(&mut self, request: grpc::Request<chat::LeaveRequest>) -> Self::LeaveRoomFuture {
        let mut conns = self.state.client_conns.lock().unwrap();

        let req = request.get_ref();
        let user_id: i32 = req.user_id.as_ref().unwrap().id;

        conns.remove(&user_id);
        //print!("test");
        let response = chat::LeaveResponse {};
        let res = grpc::Response::new(response);
        future::ok(res)
    }

    fn get_rooms_by_user(
        &mut self,
        request: grpc::Request<common::UserId>,
    ) -> Self::GetRoomsByUserFuture {
        let user_id = request.get_ref().id;

        let rooms = self.db_handler.get_rooms_by_user(user_id);

        match rooms {
            Some(r) => {
                let room_ids = r.into_iter().map(|x| chat::RoomId { id: x.id }).collect();

                let room_response = chat::Rooms { rooms: room_ids };
                return future::ok(grpc::Response::new(room_response));
            }

            None => {
                warn!("The request is invalid");
                return future::err(grpc::Status::new(
                    grpc::Code::Internal,
                    "error handling the request",
                ));
            }
        }
    }

    fn get_all_rooms(&mut self, _: grpc::Request<chat::RoomRequest>) -> Self::GetAllRoomsFuture {
        let rooms = self.db_handler.get_all_rooms();

        match rooms {
            Some(r) => {
                let room_ids = r.into_iter().map(|x| chat::RoomId { id: x.id }).collect();

                let room_response = chat::Rooms { rooms: room_ids };
                return future::ok(grpc::Response::new(room_response));
            }

            None => {
                warn!("The request is invalid");
                return future::err(grpc::Status::new(
                    grpc::Code::Internal,
                    "error handling the request",
                ));
            }
        }
    }
    fn get_last_messages_by_room(
        &mut self,
        request: grpc::Request<chat::LastMessagesRequest>,
    ) -> Self::GetLastMessagesByRoomFuture {
        let req = request.get_ref();

        let count = req.count;
        let room_id = req.room_id.clone().unwrap().id;

        let msgs = self.db_handler.get_last_messages_by_room(room_id, count);

        match msgs {
            Some(m) => {
                let msg_grpc = m
                    .into_iter()
                    .map(|x| chat::Message {
                        sender: Some(common::UserId { id: x.sender }),
                        room_id: Some(chat::RoomId { id: x.room_id }),
                        body: x.body,
                        sent_time: Some(prost_types::Timestamp::from(x.sent_time)),
                    })
                    .collect();

                let msg_response = chat::Messages { messages: msg_grpc };
                return future::ok(grpc::Response::new(msg_response));
            }

            None => {
                warn!("The request is invalid");
                return future::err(grpc::Status::new(
                    grpc::Code::Internal,
                    "error handling the request",
                ));
            }
        }
    }
}

impl ChatServe {
    pub fn new() -> ChatServe {
        println!("new service created");
        let service = ChatServe {
            state: Arc::new(State {
                client_conns: Mutex::new(HashMap::new()),
            }),
            db_handler: super::super::storage::PostgresConn::establish_connection(),
        };

        return service;
    }
}
