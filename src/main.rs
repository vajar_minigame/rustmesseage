extern crate diesel;
extern crate nats;
extern crate prost;
extern crate prost_types;
use futures::{Future, Stream};
use tokio::net::TcpListener;

use log::error;

use tower_hyper::server::{Http, Server};

use chatapp::proto::chat;

fn main() {
    println!("hello world");

    // create a message room
    //
    // create messages for testing
    //let room =chatapp::create_room(&db_connection,"blubroom","super great room");

    //let chat_msg=chatapp::create_message(&db_connection,1,SystemTime::now(),room.id,"this is a message");

    /*
        let user_id = model::UserId { id: 1 };
        let get_user = model::GetUserCall {
            user: Some(user_id),
        };
        let user_call = model::UserServiceCall {
            message: Some(model::user_service_call::Message::GetUserCall(get_user)),
        };

        let call = ctrlmsg::FuncCall {
            message: Some(ctrlmsg::func_call::Message::UserServiceCall(user_call)),
        };

        let msg = ctrlmsg::Message {
            msg: Some(ctrlmsg::message::Msg::Call(call)),
        };

        let result = messages.load::<storage::db_models::Message>(&db_connection);

        for chat_msg in result {
            println!("{:?}", chat_msg);
        }

    */
    //let mut client = Client::new("nats://127.0.0.1").unwrap();
    //let chan = client.subscribe("messagesOut", None).unwrap();

    //let mut a_byte: Vec<u8> = Vec::new();

    //prost::Message::encode(&msg, &mut a_byte).unwrap();

    //client.publish("messagesIn", &a_byte).unwrap();

    start_grpc_server();
}

fn start_grpc_server() {
    let _ = ::env_logger::init();

    use chatapp::services::chat::*;
    let handler = ChatServe::new();

    let new_service = chat::server::ChatServiceServer::new(handler);

    let mut server = Server::new(new_service);
    let http = Http::new().http2_only(true).clone();

    let addr = "127.0.0.1:10000".parse().unwrap();
    let bind = TcpListener::bind(&addr).expect("bind");

    println!("listening on {:?}", addr);

    let serve = bind
        .incoming()
        .for_each(move |sock| {
            if let Err(e) = sock.set_nodelay(true) {
                return Err(e);
            }

            let serve = server.serve_with(sock, http.clone());
            tokio::spawn(serve.map_err(|e| error!("h2 error: {:?}", e)));

            Ok(())
        })
        .map_err(|e| eprintln!("accept error: {}", e));

    tokio::run(serve);
}
