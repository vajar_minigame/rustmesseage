#!/usr/bin/env bash
protoc -I=proto/  --rust_out=src/models proto/monster.proto
protoc -I=proto/  --rust_out=src/models proto/user.proto
protoc -I=proto/  --rust_out=src/controller proto/auth.proto
protoc -I=proto/  --rust_out=src/controller proto/controller.proto
protoc -I=proto/  --rust_out=src/models proto/inventory.proto
protoc -I=proto/  --rust_out=src/models proto/interactions.proto
protoc -I=proto/  --rust_out=src/models proto/quest.proto
protoc -I=proto/  --rust_out=src/models proto/ids.proto
protoc -I=proto/  --rust_out=src/models proto/chat.proto


