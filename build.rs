use std::env;
use std::fs::{copy, read_dir};

fn main() {
    println!("hello world");
    // Build helloworld

    let protos = vec![
        "user.proto",
        "monster.proto",
        "auth.proto",
        "controller.proto",
        "inventory.proto",
        "interactions.proto",
        "quest.proto",
        "common.proto",
        "chat.proto",
    ];
    let include_folder = "proto/";

    let include_protos = protos
        .clone()
        .into_iter()
        .map(|x| include_folder.to_string() + x)
        .collect::<Vec<_>>();

    tower_grpc_build::Config::new()
        .enable_server(true)
        .enable_client(true)
        .build(&include_protos, &[include_folder.to_string()])
        .unwrap_or_else(|e| panic!("protobuf compilation failed: {}", e));

    let path = env::var_os("OUT_DIR").unwrap().into_string().unwrap();

    for f in read_dir(&path).unwrap() {
        let filename = f.unwrap().file_name();
        copy(
            path.clone() + "/" + filename.to_str().unwrap(),
            "src/proto/".to_owned() + &filename.to_str().unwrap(),
        )
        .unwrap();
    }
}
